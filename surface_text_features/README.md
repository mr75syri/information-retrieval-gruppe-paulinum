# Surface Text Features
* res: evtl. werden später Ressourcen wie Trainings-Korpora benötigt
* src:
   <ul>
    <li> <b>feature_extractors</b>: enthält für jedes Feature (siehe Wiki) eine Klasse zur Extraktion,
        pro Datei eine gleichnamige Klasse (siehe Beispiel Sent_Length_Extractor.py). Jede dieser Klassen
          erbt von der Klasse Extractor_Base_Class und implementiert deren zwei Methoden </li>
   <li> <b>tools</b>: Idee ist, dass wir in diesem Package alle Methoden ablegen, die potenziell überall mal
        benötigt werden könnten. Hauptsächlich werden das NLP-Methoden sein, dafür existiert schon ein
        Modul nlp.py. Das dient nur noch zu Testzwecken, der NLP-verarbeitete Korpus liegt in res/nlp_cache </li>
    <li> <b>Extractor_Base_Class.py </b> enthält die abstrakte Basisklasse für jeden Feature Extractor </li> 
	<li> <b> checkpoints </b> enthält eine Datei für "Checkpoints" der einzelnen Extraktoren (Extraktionsskript kann unterbrochen werden) 
			und für jeden Extraktor eine Datei mit den extrahierten Werten (z.B. "Sent_Length_args-.csv")
   </ul>