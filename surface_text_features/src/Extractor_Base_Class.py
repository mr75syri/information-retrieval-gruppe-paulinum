from abc import ABC, abstractmethod, abstractproperty

""" an abstract base class for all feature extractors 
    all feature extractors inherit from this class and implement its methods 
"""
class Feature_Extractor(ABC):

    """ the method to extract the feature from exactly one argument
    Args:
        text_raw: the raw text of the argument
        text_tokenized: list of list of words (argument already segmented and tokenized using a standard nltk approach)
            (reason: speed up the process)
        pos_list: list of list of (word,pos) tuples for the tokenized text
        lemma_list: list of list of lemmas for the tokenized text
    Returns:
        a numeric value (the feature). Need not be scaled or standardized
    """
    @abstractmethod
    def extract(self, text_raw, text_tokenized, pos_list, lemma_list):
        pass


    """ a not very beautiful approach to enforcing a name for every extractor
    Returns: 
        a string describing the name of the feature extracted
    """
    @abstractmethod
    def get_name(self):
        pass




