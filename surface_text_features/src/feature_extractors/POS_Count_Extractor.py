# import the base class
from Extractor_Base_Class import Feature_Extractor

""" Feature_Extractor to extract feature 6 from the wiki 
    inherits Feature_Extractor as every Extractor class must 
"""
class POS_Count_Extractor(Feature_Extractor):

    # override the extract method (see Extractor_Base_Class.py for detailed documentation)

    def extract(self, text_raw, text_tokenized, pos_list, lemma_list):
        # first collect all POS-Tags...
        pos_tags = []
        for sent in pos_list:
            #print(sent)
            for (word, tag) in sent:
                # punctuation is of the form (":",":") -> do not count punctuation
                if word!=tag:
                    pos_tags.append(tag)
        # ...then remove duplicates
        return len(set(pos_tags))


    # override the get_name method (see Extractor_Base_Class.py for detailed documentation)
    def get_name(self):
        return "Distinct_POS_Count"







