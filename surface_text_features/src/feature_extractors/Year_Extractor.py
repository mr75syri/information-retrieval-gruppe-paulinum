# import the base class
from Extractor_Base_Class import Feature_Extractor
import re

""" Extractor to find mention of years (1000 - 2019)
"""


class Year_Extractor(Feature_Extractor):

    def __init__(self):
        super(Year_Extractor, self).__init__()
        regexs = [r"[^\w]1[1-9][0-9][0-9][^\w]", r"[^\w]20[0-1][0-9][^w]"]
        self.regexes= [re.compile(regex) for regex in regexs]

    def extract(self, text_raw, text_tokenized, pos_list, lemma_list):
        if(len(text_tokenized)==0):
            return 0
        counter = 0
        for r in self.regexes:
            counter += len(r.findall(text_raw))
            #print(counter)
        return counter/len(text_tokenized)



    def get_name(self):
        return "Years"