# import the base class
from Extractor_Base_Class import Feature_Extractor
import re

""" Extractor to find reasonable percent numbers in text (not: 100%, 0%, 0,x%, 99,9999%)
"""


class Percentage_Extractor(Feature_Extractor):

    def __init__(self):
        super(Percentage_Extractor, self).__init__()
        self.regex= re.compile(r"[1-9][0-9]?,[0-9]{0,2}%")

    def extract(self, text_raw, text_tokenized, pos_list, lemma_list):
        if(len(text_tokenized)==0):
            return 0

        return len(self.regex.findall(text_raw))/len(text_tokenized)



    def get_name(self):
        return "Percentage"