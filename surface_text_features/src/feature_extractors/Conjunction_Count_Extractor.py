# import the base class
from Extractor_Base_Class import Feature_Extractor
from tools.nlp import tokenize


""" Feature_Extractor to extract feature 7 from the wiki (number of different conjunctions overall)
    inherits Feature_Extractor as every Extractor class must 
"""
class Conjunction_Count_Extractor(Feature_Extractor):

    # override the extract method (see Extractor_Base_Class.py for detailed documentation)
    def __init__(self):
        super(Conjunction_Count_Extractor, self).__init__()
        # conjunctions can have different lengths
        conj = []
        with open("../res/wordlists/conjunctions") as file:
            for c in file:
                print(c.strip())
                conj.append(c.strip())
        # conjunctions can have different lengths
        lengths = [len(c.split(" ")) for c in conj]
        self.max_length = max(lengths)
        #print(self.max_length)
        # list of lists of conjunctions (conjunctions in list at index i have length i+1)
        self.conjunctions = []
        self.conjunctions.append([])
        for i in range(1,self.max_length+1):
            self.conjunctions.append([])
            self.conjunctions[i]= [c for c in conj if len(c.split())==i]
        #print(self.conjunctions)

    def extract(self, text_raw, text_tokenized, pos_list, lemma_list):
        # first collect all conjunctions...
        conj_list = []
        #print("and" in self.conjunctions[1])
        # conjunctions can have different lengths...
        for i in range(1,self.max_length+1):
            for sent in text_tokenized:
                for ind in range(len(sent)-i+1):
                    # build the candidate for a conjunction, i.e. a i-gram
                    candidate = ""
                    for j in range(ind, ind+i):
                        candidate+=sent[j].lower()
                        candidate+=" "
                    candidate = candidate.strip()
                    #print("Testing for",candidate)
                    #if candidate == "and":
                        #print("found and")
                    if candidate in self.conjunctions[i]:
                        #print("appended ",candidate)
                        conj_list.append(candidate)
        # unique conjunctions, normalize per sentence
        if(len(text_tokenized)==0):
            return 0
        return len(set(conj_list))/len(text_tokenized)


    # override the get_name method (see Extractor_Base_Class.py for detailed documentation)
    def get_name(self):
        return "Conjunctions"







