# import the base class
from Extractor_Base_Class import Feature_Extractor
from tools.nlp import tokenize
from os.path import isfile


""" Feature_Extractor to extract feature 7 from the wiki (number of different hedge words overall)
    inherits Feature_Extractor as every Extractor class must 
"""
class Hedge_Words_Extractor(Feature_Extractor):

    # override the extract method (see Extractor_Base_Class.py for detailed documentation)
    def __init__(self):
        super(Hedge_Words_Extractor, self).__init__()
        # hedge words can have different lengths
        hedge = []
        filepath = None
        if isfile("../res/wordlists/hedges"):
            filepath = "../res/wordlists/hedges"
        elif isfile("../../../res/wordlists/hedges"):
            filepath = "../../../res/wordlists/hedges"
        with open(filepath) as file:
            for c in file:
                # print(c.strip())
                hedge.append(c.strip())
        # hedge words can have different lengths
        lengths = [len(c.split(" ")) for c in hedge]

        self.max_length = max(lengths)
        #print(self.max_length)
        # list of lists of hedge words (hedge words in list at index i have length i+1)
        self.hedges = []
        self.hedges.append([])
        for i in range(1,self.max_length+1):
            self.hedges.append([])
            self.hedges[i]= [c for c in hedge if len(c.split())==i]
        #print(self.hedge words)

    def extract(self, text_raw, text_tokenized, pos_list, lemma_list):
        # first collect all hedge words...
        hedge_list = []
        #print("and" in self.hedges[1])
        # hedge words can have different lengths...
        for i in range(1,self.max_length+1):
            for sent in text_tokenized:
                for ind in range(len(sent)-i+1):
                    # build the candidate for a conjunction, i.e. a i-gram
                    candidate = ""
                    for j in range(ind, ind+i):
                        candidate+=sent[j].lower()
                        candidate+=" "
                    candidate = candidate.strip()
                    #print("Testing for",candidate)
                    #if candidate == "and":
                        #print("found and")
                    if candidate in self.hedges[i]:
                        #print("appended ",candidate)
                        hedge_list.append(candidate)
        # normalize per sentence
        if len(text_tokenized)==0:
            return 0
        else:
            return len(set(hedge_list))/len(text_tokenized)


    # override the get_name method (see Extractor_Base_Class.py for detailed documentation)
    def get_name(self):
        return "Hedge_Words"







