# import the base class
from Extractor_Base_Class import Feature_Extractor


""" Feature_Extractor to extract feature 11 from the wiki 
    inherits Feature_Extractor as every Extractor class must 
"""
class First_Person_Plural_Extractor(Feature_Extractor):

    def __init__(self):
        super(First_Person_Plural_Extractor, self).__init__()
        # first person plural cannot be determined via POS-Tags, thus list of words
        self.pl_words = ["we","us","our","ours","ourselves"]

    # override the extract method (see Extractor_Base_Class.py for detailed documentation)
    def extract(self, text_raw, text_tokenized, pos_list, lemma_list):
        # count modal verbs with the tag "MD"
        counter = 0
        # normalize over number of tokens
        tokens = 0
        for sent in text_tokenized:
            for word in sent:
                tokens+=1
                if word.lower() in self.pl_words:
                    counter+=1
        if tokens>0:
            return counter/tokens
        else:
            return 0

    # override the get_name method (see Extractor_Base_Class.py for detailed documentation)
    def get_name(self):
        return "First_Person_Pl"







