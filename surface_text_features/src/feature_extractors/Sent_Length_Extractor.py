# import the base class
from Extractor_Base_Class import Feature_Extractor
from tools.nlp import tokenize
from tools.nlp import remove_punctuation_from_tokens

""" Feature_Extractor to extract feature 1 from the wiki 
    inherits Feature_Extractor as every Extractor class must 
"""
class Sent_Length_Extractor(Feature_Extractor):

    # override the extract method (see Extractor_Base_Class.py for detailed documentation)
    def extract(self, text_raw, text_tokenized, pos_list, lemma_list):
        # use the already tokenized version of the argument (comes als list of lists)
        # get rid of punctuation
        text_tokenized = remove_punctuation_from_tokens(text_tokenized)
        lengths = [len(sent) for sent in text_tokenized]
        if len(lengths)>0:
            return sum(lengths)/len(lengths)
        else:
            return 0

    # override the get_name method (see Extractor_Base_Class.py for detailed documentation)
    def get_name(self):
        return "Sent_Length"







