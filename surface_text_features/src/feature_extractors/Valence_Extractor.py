# import the base class
from Extractor_Base_Class import Feature_Extractor
import csv
import os

""" Feature Extractor
    Measure the average "valence" of words
"""


class Valence_Extractor(Feature_Extractor):

    def __init__(self):
        super(Valence_Extractor, self).__init__()
        self.wordDict = {}

        path = os.path.join(os.path.abspath(__file__ + "/../../../"), "res", "papers22_23", "valence.csv")

        if os.path.isfile(path):
            pass
        elif os.path.isfile("../../../res/papers22_23/valence.csv"):
            path = "../../../res/papers22_23/valence.csv"
        else:
            print("Cannot find file!")

        with open(path, "r") as csv_file:
            csv_reader = csv.DictReader(csv_file)
            for row in csv_reader:
                # note: entry might contain an empty word or 2-word units (ignore...)
                if row["word"] != "" and row["word"].find(" ") == -1:
                    self.wordDict[row["word"]] = row["valence"]

    def extract(self, text_raw, text_tokenized, pos_list, lemma_list):
        tokens = len(text_raw.split(" "))
        if tokens == 0:
            return 0

        value = 0.0  # sum of all values
        counter = 0  # how many occurrences to divide by

        for sent in lemma_list:
            for word in sent:
                word = word.lower()
                if word in self.wordDict:
                    value += float(self.wordDict[word])
                    # print(word, float(self.wordDict[word]))
                    counter += 1

        # make sure we do not divide by 0
        if counter == 0:
            return 0
        return value/counter

    def get_name(self):
        return "Valence"
