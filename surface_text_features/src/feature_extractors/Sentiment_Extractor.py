from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer

# import the base class
from Extractor_Base_Class import Feature_Extractor
import re

""" Feature Extractor for Sentiment analysis 
"""


class Sentiment_Extractor(Feature_Extractor):

    def __init__(self):
        super(Sentiment_Extractor, self).__init__()
        # TODO: make a separate word list
        self.analyzer = SentimentIntensityAnalyzer()

    def extract(self, text_raw, text_tokenized, pos_list, lemma_list):
        scores = self.analyzer.polarity_scores(text_raw)
        return scores["compound"]

    def get_name(self):
        return "Sentiment"


