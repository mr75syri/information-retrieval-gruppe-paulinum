# import the base class
from Extractor_Base_Class import Feature_Extractor
from tools.nlp import tokenize
from tools.nlp import remove_punctuation_from_tokens

""" Feature_Extractor to extract feature 5 from the wiki 
    inherits Feature_Extractor as every Extractor class must 
"""
class Interpunction_Count_Extractor(Feature_Extractor):

    def __init__(self):
        super(Interpunction_Count_Extractor, self).__init__()
        self.interpunction = [",",";",":","-"]

    # override the extract method (see Extractor_Base_Class.py for detailed documentation)
    def extract(self, text_raw, text_tokenized, pos_list, lemma_list):
        counter = 0
        # first flatten the text: an emoji like :-D could be split in two sentences
        tokens = []
        for sent in text_tokenized:
            for word in sent:
                tokens.append(word)
        #print(len(tokens), " Tokens")
        for ind in range(len(tokens)):
            if tokens[ind] in self.interpunction:
                #trying to filter out emojis etc.: do not count sequences of interpunction characters
                # (of course this doesnt always work
                # what is more, filter out : in URLs
                if (ind>0 and (tokens[ind-1] in self.interpunction or tokens[ind-1]=="http" or tokens[ind-1]=="https")) or \
                            ((ind<len(tokens)-1) and tokens[ind+1][0] in self.interpunction):
                    continue
                else:
                    counter+=1
        if len(text_tokenized)>0:
            return counter/len(text_tokenized)
        else:
            return 0


    # override the get_name method (see Extractor_Base_Class.py for detailed documentation)
    def get_name(self):
        return "Interpunction_Count"







