# import the base class
from Extractor_Base_Class import Feature_Extractor


""" Feature_Extractor to extract feature 10 from the wiki 
    inherits Feature_Extractor as every Extractor class must 
"""
class Article_Ratio_Extractor(Feature_Extractor):


    # override the extract method (see Extractor_Base_Class.py for detailed documentation)
    def extract(self, text_raw, text_tokenized, pos_list, lemma_list):
        # defined and undefined articles
        defined = 0
        undefined = 0
        for sent in text_tokenized:
            for word in sent:
                word = word.lower()
                if word=="the":
                    defined+=1
                elif word=="a" or word=="an":
                    undefined+=1
        if defined+undefined > 0:
            return defined/(defined+undefined)
        else:
            return 0

    # override the get_name method (see Extractor_Base_Class.py for detailed documentation)
    def get_name(self):
        return "Article_Ratio"







