""" not really a test, more a sanity check"""

from feature_extractors.Percentage_Extractor import Percentage_Extractor
from tools.nlp import *
test_sents = [
    "A sentence without any numbers.",
    "This sentence is 100% retarded and contains 0% information.",
    "This sentence speaks of 28,25%. However, this one tells us that it's really 99,9999%.",
    "50% of these sentences are nonsense while 75,00% of them contain percentiles."
]

extractor = Percentage_Extractor()
print("Testing ", extractor.get_name())

for ind in range(len(test_sents)):
    nlp = nlp_pipe(test_sents[ind])
    print(ind, extractor.extract(nlp[RAW], nlp[TOKENIZED], nlp[TAGGED], nlp[LEMMAS]))