from tools.nlp import  *
from feature_extractors.Interpunction_Count_Extractor import Interpunction_Count_Extractor

test_sents = [
    "This is a sentence without any relevant interpunction.",
    "Here, several relevant interpunction characters are used: some commas, a colon; a semicolon and - a hyphen.",
    "Some stupid sentence which contains an emoji :-D.",
    "Important: URLs like http://www.uni-leipzig.de should not be considered a punctuation, even though nltk extracts the colon."
]


extractor = Interpunction_Count_Extractor()
print("Testing ", extractor.get_name())

for ind in range(len(test_sents)):
    nlp = nlp_pipe(test_sents[ind])
    print(ind, extractor.extract(nlp[RAW], nlp[TOKENIZED], nlp[TAGGED], nlp[LEMMAS]))