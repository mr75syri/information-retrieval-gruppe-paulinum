from feature_extractors.Examples_Extractor import Examples_Extractor
from tools.nlp import *
test_sents = [
    "Here is an example: e.g. xyz",
    "For example, consider abc, namely xyz.",
    "A sentence containing no example.",
    "This sentence contains no example, ergo it should get a zero rating."
]

extractor = Examples_Extractor()
print("Testing ", extractor.get_name())

for ind in range(len(test_sents)):
    nlp = nlp_pipe(test_sents[ind])
    print(ind, extractor.extract(nlp[RAW], nlp[TOKENIZED], nlp[TAGGED], nlp[LEMMAS]))
