from feature_extractors.Citation_Count_Extractor import Citation_Count_Extractor
from tools.nlp import *
test_sents = [
    "Here, there are no citations.",
    "This sentence ([1]) includes several citations [2],[12],[345]. The same is true for this [6] sentence (cf. [7],[42]).",
    "Here, only the rather rare page citations (cf. p.198) are employed. See e.g. on p. 12 and p.9, furthermore here (p.2112).",
    "This is the last test text([12]). It contains both standard ([23][1][10]) citations as well as page (p.123) citations. However, "
    "the latter (e.g. p. 323) should not be counted (cf. p.99)[2]."
]

extractor = Citation_Count_Extractor()
print("Testing ", extractor.get_name())

for ind in range(len(test_sents)):
    nlp = nlp_pipe(test_sents[ind])
    print(ind, extractor.extract(nlp[RAW], nlp[TOKENIZED], nlp[TAGGED], nlp[LEMMAS]))