""" not really a test, more a sanity check"""

from feature_extractors.Sent_Length_Extractor import Sent_Length_Extractor
from tools.nlp import *
test_sents = [
    "This is a test sentence. It is used to test the class.",
    "This argument contains just one sentence, however, this sentence is way longer than the one above.",
    "Three. Short. Sentences."
]

extractor = Sent_Length_Extractor()
print("Testing ", extractor.get_name())

for ind in range(len(test_sents)):
    nlp = nlp_pipe(test_sents[ind])
    print(ind, extractor.extract(nlp[RAW], nlp[TOKENIZED], nlp[TAGGED], nlp[LEMMAS]))