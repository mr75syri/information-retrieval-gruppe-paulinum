from feature_extractors.Sentiment_Extractor import Sentiment_Extractor
from tools.nlp import *
test_sents = [
    "This sentence should not be very special.",
    "I am happy to show you this beautiful, lovely sentence.",
    "Fuck this shit. Here are two stupid sentences."
]

extractor = Sentiment_Extractor()
print("Testing ", extractor.get_name())

for ind in range(len(test_sents)):
    nlp = nlp_pipe(test_sents[ind])
    print(ind, extractor.extract(nlp[RAW], nlp[TOKENIZED], nlp[TAGGED], nlp[LEMMAS]))