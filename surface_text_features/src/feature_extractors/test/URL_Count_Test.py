from feature_extractors.URL_Count_Extractor import URL_Count_Extractor
from tools.nlp import *

test_sents = [
    "In this sentence, the URL http://www.uni-leipzig.de is used to check if the extractor gets it.",
    "Here, another one, namely https://www.uni-ulm.de, is presented. However, now there are two sentences.",
    "In this piece of text, no URL is contained."
]

extractor = Spam_Ratio_Extractor()
print("Testing ", extractor.get_name())

for ind in range(len(test_sents)):
    nlp = nlp_pipe(test_sents[ind])
    print(ind, extractor.extract(nlp[RAW], nlp[TOKENIZED], nlp[TAGGED], nlp[LEMMAS]))