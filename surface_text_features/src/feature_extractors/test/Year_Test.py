""" not really a test, more a sanity check"""

from feature_extractors.Year_Extractor import Year_Extractor
from tools.nlp import *
test_sents = [
    "A sentence without any years.",
    "WWII was from 1939-1945.",
    "Germany won the football world cup in 1954, 1974, 1990 and 2014. In both 1938 and 2018 they failed at the first "
    "round."
]

extractor = Year_Extractor()
print("Testing ", extractor.get_name())

for ind in range(len(test_sents)):
    nlp = nlp_pipe(test_sents[ind])
    print(ind, extractor.extract(nlp[RAW], nlp[TOKENIZED], nlp[TAGGED], nlp[LEMMAS]))