from feature_extractors.Second_Person_Extractor import Second_Person_Extractor
from tools.nlp import *
test_sents = [
    "In your sentence here, you use many pronouns for yourselves.",
    "This sentence does not contain second person pronouns.",
    "Here, one sentence contains such pronouns. You believe it is the second one.",
    "This is the last test sentence of yours, containing one relevant pronoun.",
    "You have no fans You have no fans You have no fans You have no fans You have no fans"
]

extractor = Second_Person_Extractor()
print("Testing ", extractor.get_name())

for ind in range(len(test_sents)):
    nlp = nlp_pipe(test_sents[ind])
    print(ind, extractor.extract(nlp[RAW], nlp[TOKENIZED], nlp[TAGGED], nlp[LEMMAS]))