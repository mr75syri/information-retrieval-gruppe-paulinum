from feature_extractors.Non_Stopword_Extractor import Non_Stopword_Extractor
from tools.nlp import *
test_sents = [
    "This is a test sentence. It is used to test the class.",
    "This sentence here has many very common words and because of that its rating will be low.",
    "An extraordinary piece of text containing fancy words.",
    "Here there are two sentences. One of them includes more stopwords that the other, obviously."
]

extractor = Non_Stopword_Extractor()
print("Testing ", extractor.get_name())

for ind in range(len(test_sents)):
    nlp = nlp_pipe(test_sents[ind])
    print(ind, extractor.extract(nlp[RAW], nlp[TOKENIZED], nlp[TAGGED], nlp[LEMMAS]))