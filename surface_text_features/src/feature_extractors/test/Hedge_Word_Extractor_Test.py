from feature_extractors.Hedge_Words_Extractor import Hedge_Words_Extractor
from tools.nlp import *
test_sents = [
    "This sentence does not contain a hedge word.",
    "A generally somehow unlikely sentence, possibly containing kind of many hedges in my understanding.",
    "Two perhaps interesting sentences. Evidently, both of them appear to be fairly dumb."
]

extractor = Hedge_Words_Extractor()
print("Testing ", extractor.get_name())

for ind in range(len(test_sents)):
    nlp = nlp_pipe(test_sents[ind])
    print(ind, extractor.extract(nlp[RAW], nlp[TOKENIZED], nlp[TAGGED], nlp[LEMMAS]))