from feature_extractors.Emoji_Extractor import Emoji_Extractor
from tools.nlp import *
test_sents = [
    "This example does not have any emoji or shit.",
    "However, I do like some of them here: :D, ^^ and I do not like xD",
    ":D:D:D xD",
    "The value is <300, as shown in the URL abc=D8Dxc:(xyz):Da:Oabc."
]

extractor = Emoji_Extractor()
print("Testing ", extractor.get_name())

for ind in range(len(test_sents)):
    nlp = nlp_pipe(test_sents[ind])
    print(ind, extractor.extract(nlp[RAW], nlp[TOKENIZED], nlp[TAGGED], nlp[LEMMAS]))
