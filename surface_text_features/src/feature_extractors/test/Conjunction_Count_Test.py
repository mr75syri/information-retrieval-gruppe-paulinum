from feature_extractors.Conjunction_Count_Extractor import Conjunction_Count_Extractor
from tools.nlp import *
test_sents = [
    "A sentence without conjunctions is not considered elegant.",
    "However, a text containing many conjunctions is thought of as good because it displays rhetoric capabilities. What "
    "is more, it is convenient to read even though it might sound exaggerated.",
    "This sounds stupid and should get a low rating: a A and a B and a C and..."
]

extractor = Conjunction_Count_Extractor()
print("Testing ", extractor.get_name())

for ind in range(len(test_sents)):
    nlp = nlp_pipe(test_sents[ind])
    print(ind, extractor.extract(nlp[RAW], nlp[TOKENIZED], nlp[TAGGED], nlp[LEMMAS]))