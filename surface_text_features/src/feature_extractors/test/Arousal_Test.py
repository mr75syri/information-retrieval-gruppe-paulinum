from feature_extractors.Arousal_Extractor import Arousal_Extractor
from tools.nlp import *
test_sents = [
    "For this test we will need to be more precise.",
    "I feel the lust deeply inside my body, accelerating every single cell.",
    "If you look closely, these sentences are not new at all."
]

extractor = Arousal_Extractor()
print("Testing ", extractor.get_name())

for ind in range(len(test_sents)):
    nlp = nlp_pipe(test_sents[ind])
    print(ind, extractor.extract(nlp[RAW], nlp[TOKENIZED], nlp[TAGGED], nlp[LEMMAS]))
