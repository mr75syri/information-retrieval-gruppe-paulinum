from feature_extractors.First_Person_Plural_Extractor import First_Person_Plural_Extractor
from tools.nlp import *
test_sents = [
    "In our sentence here, we use many pronouns for ourselves.",
    "This sentence does not contain first person pronouns.",
    "Here, one sentence contains such pronouns. We believe it is the second one.",
    "This is the last test sentence of ours, containing one relevant pronoun."
]

extractor = First_Person_Plural_Extractor()
print("Testing ", extractor.get_name())

for ind in range(len(test_sents)):
    nlp = nlp_pipe(test_sents[ind])
    print(ind, extractor.extract(nlp[RAW], nlp[TOKENIZED], nlp[TAGGED], nlp[LEMMAS]))