from feature_extractors.Modal_Verb_Count_Extractor import Modal_Verb_Count_Extractor
from tools.nlp import *
test_sents = [
    "This is a test sentence. It is used to test the class.",
    "This sentence should get a rating of one as it contains one modal verb.",
    "Only one of these two contain a modal verb. This sentence may be the one.",
    "It must be noted that this sentence can get a high rating because it might contain more modal verbs than it should."
]

extractor = Modal_Verb_Count_Extractor()
print("Testing ", extractor.get_name())

for ind in range(len(test_sents)):
    nlp = nlp_pipe(test_sents[ind])
    print(ind, extractor.extract(nlp[RAW], nlp[TOKENIZED], nlp[TAGGED], nlp[LEMMAS]))