from tools.nlp import  *
from feature_extractors.Word_Length_Extractor import Word_Length_Extractor

test_sents = [
    "This is a test sentence. It is used to test the class.",
    "This argument contains just one sentence, however, this sentence is way longer than the one above.",
    "Three. Short. Sentences."
]


extractor = Word_Length_Extractor()
print("Testing ", extractor.get_name())

for ind in range(len(test_sents)):
    nlp = nlp_pipe(test_sents[ind])
    print(ind, extractor.extract(nlp[RAW], nlp[TOKENIZED], nlp[TAGGED], nlp[LEMMAS]))