""" not really a test, more a sanity check"""

from feature_extractors.POS_Count_Extractor import POS_Count_Extractor
from tools.nlp import *
test_sents = [
    "This is a test sentence. It is used to test the class.",
    "This beautiful sentence, which is very well written, contains a lot of different tags.",
    "The number of tags is measured across the whole text. Although it may seem strange, this makes perfectly sense."
]

extractor = POS_Count_Extractor()
print("Testing ", extractor.get_name())

for ind in range(len(test_sents)):
    nlp = nlp_pipe(test_sents[ind])
    print(ind, extractor.extract(nlp[RAW], nlp[TOKENIZED], nlp[TAGGED], nlp[LEMMAS]))