from feature_extractors.Article_Ratio_Extractor import Article_Ratio_Extractor
from tools.nlp import *
test_sents = [
    "A sentence without any articles.",
    "The sentence here contains only the definite article. The same is true for this one.",
    "A sentence in which the definite article can be found, as well as an indefinite.",
    "A sentence containing only an indefinite article"
]

extractor = Article_Ratio_Extractor()
print("Testing ", extractor.get_name())

for ind in range(len(test_sents)):
    nlp = nlp_pipe(test_sents[ind])
    print(ind, extractor.extract(nlp[RAW], nlp[TOKENIZED], nlp[TAGGED], nlp[LEMMAS]))