from feature_extractors.Concreteness_Extractor import Concreteness_Extractor
from tools.nlp import *
test_sents = [
    "For this test we will need to be more precise.",
    "Perhaps we want to add another sentence that contains more words.",
    "For mysterious reasons this sentence contains the phrase a cappella.",
]

extractor = Concreteness_Extractor()
print("Testing ", extractor.get_name())

for ind in range(len(test_sents)):
    nlp = nlp_pipe(test_sents[ind])
    print(ind, extractor.extract(nlp[RAW], nlp[TOKENIZED], nlp[TAGGED], nlp[LEMMAS]))
