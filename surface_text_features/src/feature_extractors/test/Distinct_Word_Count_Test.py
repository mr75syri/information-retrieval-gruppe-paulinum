from tools.nlp import  *
from feature_extractors.Distinct_Word_Count_Extractor import Distinct_Word_Count_Extractor

test_sents = [
    "These are some different words, but most of them stopwords.",
    "A very stupid sentence. This sentence is stupid and because it repeats the word 'stupid' several times, it is stupid.",
    "A sentence with a great variety of used words should definitely get an impressing ranking in this test."
]


extractor = Distinct_Word_Count_Extractor()
print("Testing ", extractor.get_name())

for ind in range(len(test_sents)):
    nlp = nlp_pipe(test_sents[ind])
    print(ind, extractor.extract(nlp[RAW], nlp[TOKENIZED], nlp[TAGGED], nlp[LEMMAS]))