from feature_extractors.Dominance_Extractor import Dominance_Extractor
from tools.nlp import *
test_sents = [
    "For this test we will need to be more precise.",
    "Perhaps we want to add another sentence that contains more words.",
    "Determination and detail is the key here.",
    "If you look closely, these sentences are not new at all."
]

extractor = Dominance_Extractor()
print("Testing ", extractor.get_name())

for ind in range(len(test_sents)):
    nlp = nlp_pipe(test_sents[ind])
    print(ind, extractor.extract(nlp[RAW], nlp[TOKENIZED], nlp[TAGGED], nlp[LEMMAS]))
