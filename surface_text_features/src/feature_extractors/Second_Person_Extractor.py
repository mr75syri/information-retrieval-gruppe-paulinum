# import the base class
from Extractor_Base_Class import Feature_Extractor


""" Feature_Extractor to extract feature 11 from the wiki 
    inherits Feature_Extractor as every Extractor class must 
"""
class Second_Person_Extractor(Feature_Extractor):

    def __init__(self):
        super(Second_Person_Extractor, self).__init__()
        # 2nd person cannot be determined via POS-Tags, thus list of words
        self.words = ["you","your","yours","yourselves", "yourself"]

    # override the extract method (see Extractor_Base_Class.py for detailed documentation)
    def extract(self, text_raw, text_tokenized, pos_list, lemma_list):
        # count modal verbs with the tag "MD"
        counter = 0
        num_tokens = 0
        for sent in text_tokenized:
            for word in sent:
                num_tokens+=1
                if word.lower() in self.words:
                    counter+=1
        if len(pos_list)>0:
            return counter/num_tokens
        else:
            return 0

    # override the get_name method (see Extractor_Base_Class.py for detailed documentation)
    def get_name(self):
        return "Second_Person"







