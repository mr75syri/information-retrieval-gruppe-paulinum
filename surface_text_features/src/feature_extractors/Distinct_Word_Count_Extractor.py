# import the base class
from Extractor_Base_Class import Feature_Extractor
from tools.nlp import tokenize
from tools.nlp import remove_punctuation_from_tokens
from nltk.corpus import stopwords

""" Feature_Extractor to extract feature 4 from the wiki 
    inherits Feature_Extractor as every Extractor class must 
"""
class Distinct_Word_Count_Extractor(Feature_Extractor):

    # override the extract method (see Extractor_Base_Class.py for detailed documentation)
    def __init__(self):
        super(Distinct_Word_Count_Extractor, self).__init__()
        self.stopwords = stopwords.words("english")

    def extract(self, text_raw, text_tokenized, pos_list, lemma_list):
        # only word tokens
        text_tokenized = remove_punctuation_from_tokens(text_tokenized)
        # count distinct non-stopwords, normalize by text length (text without stopwords)
        length = 0
        words = []
        # first collect all relevant words in a list ...
        for sent in text_tokenized:
            for word in sent:
                if word not in self.stopwords:
                    words.append(word)
                    length+=1
        # ... then remove duplicates
        word_set = set(words)
        if length > 0:
            return len(word_set)/length
        else:
            return 0


    # override the get_name method (see Extractor_Base_Class.py for detailed documentation)
    def get_name(self):
        return "Distinct_Word_Count"







