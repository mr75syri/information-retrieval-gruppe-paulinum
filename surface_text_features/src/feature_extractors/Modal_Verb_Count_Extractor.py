# import the base class
from Extractor_Base_Class import Feature_Extractor


""" Feature_Extractor to extract feature 1 from the wiki 
    inherits Feature_Extractor as every Extractor class must 
"""
class Modal_Verb_Count_Extractor(Feature_Extractor):

    # override the extract method (see Extractor_Base_Class.py for detailed documentation)
    def extract(self, text_raw, text_tokenized, pos_list, lemma_list):
        # count modal verbs with the tag "MD"
        counter = 0
        for sent in pos_list:
            for (word, tag) in sent:
                if tag=="MD":
                    counter+=1
        if len(pos_list)>0:
            return counter/len(pos_list)
        else:
            return 0

    # override the get_name method (see Extractor_Base_Class.py for detailed documentation)
    def get_name(self):
        return "Modal_Verbs"







