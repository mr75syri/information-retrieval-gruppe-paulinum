# import the base class
from Extractor_Base_Class import Feature_Extractor

""" Feature_Extractor to extract feature 3 from the wiki 
    inherits Feature_Extractor as every Extractor class must 
"""
class URL_Count_Extractor(Feature_Extractor):

    # override the extract method (see Extractor_Base_Class.py for detailed documentation)
    def extract(self, text_raw, text_tokenized, pos_list, lemma_list):
        # checked the first ~100 .com URLs in argsme.json. All of them are of the form "http[s]:..."
        # the tokenizer cuts such URLs in several tokens: "http[s]",":","..."
        counter = 0
        for sent in text_tokenized:
            for ind in range(len(sent)-1):
                word = sent[ind]
                if word=="http" or word=="https" and sent[ind+1]==":":
                    counter+=1
        if counter>0:
            return counter/len(text_tokenized)
        else:
            return 0



    # override the get_name method (see Extractor_Base_Class.py for detailed documentation)
    def get_name(self):
        return "URLs"







