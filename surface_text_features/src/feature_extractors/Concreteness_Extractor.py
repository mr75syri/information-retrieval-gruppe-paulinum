# import the base class
from Extractor_Base_Class import Feature_Extractor
import csv
import os
import time

""" Feature Extractor
    Measure concreteness of words
    using a prepared CSV list with id,word,score
    labeled id,word,conc
"""


class Concreteness_Extractor(Feature_Extractor):

    def __init__(self):
        super(Concreteness_Extractor, self).__init__()
        self.wordDict = {}

        path = os.path.join(os.path.abspath(__file__ + "/../../../"), "res", "papers22_23", "conc.csv")

        if os.path.isfile(path):
            # print("(use first path)")
            pass
        elif os.path.isfile("../../../res/papers22_23/conc.csv"):
            path = "../../../res/papers22_23/conc.csv"
            # print("(use second path)")
        else:
            print("Cannot find file!")

        with open(path, "r") as csv_file:
            csv_reader = csv.DictReader(csv_file)
            for row in csv_reader:
                # note: 21730 contains an empty "word"... ignoring this row
                if row["word"] != "":
                    self.wordDict[row["word"]] = row["conc"]

    def extract(self, text_raw, text_tokenized, pos_list, lemma_list):
        tokens = len(text_raw.split(" "))
        if tokens == 0:
            return 0

        conc_value = 0.0  # sum of all conc values
        counter = 0  # how many occurrences to divide by

        # start_time = time.time()

        for sent in lemma_list:
            for word in sent:
                word = word.lower()
                if word in self.wordDict:
                    conc_value += float(self.wordDict[word])
                    # print(word, float(self.wordDict[word]))
                    counter += 1

        """
        with open(path, "r") as csv_file:
            csv_reader = csv.DictReader(csv_file)

            for row in csv_reader:
                # note: "37058,a cappella,2.92" is the first row with two words
                # note: 21730 contains an empty "word"... ignoring this row
                if row_index >= 37058:
                    if text_raw.lower().find(row["word"]) > -1:
                        # print("type 2:", row["word"], row_index, row["conc"])
                        conc_value += float(row["conc"])
                        counter += 1
                elif row_index != 21730:
                    # use lemma_list to match against word stems
                    for sent in lemma_list:
                        for word in sent:
                            #if word.find(row["word"]) > -1:
                            if word.lower() == row["word"]:
                                # print("type 1:", row["word"], row_index, row["conc"])
                                conc_value += float(row["conc"])
                                counter += 1

                row_index += 1
        """
        # print("time:", time.time() - start_time)
        # make sure we do not divide by 0
        if counter == 0:
            return 0
        return conc_value/counter

    def get_name(self):
        return "Concreteness"
