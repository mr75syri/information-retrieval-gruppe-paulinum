# import the base class
from Extractor_Base_Class import Feature_Extractor
import re

""" Feature_Extractor to extract feature 1 from the wiki 
    inherits Feature_Extractor as every Extractor class must 
"""
class Citation_Count_Extractor(Feature_Extractor):

    def __init__(self):
        super(Citation_Count_Extractor, self).__init__()
        # idea: first search for citations like [1], [2] etc.
        self.regex1 = re.compile("\[[1-9][0-9]*\]")
        # then look for citations like p.123 iff no [1],[2] etc. found
        self.regex2 = re.compile("[\s\(]p\.\s?[1-9][0-9]{,3}")
    # override the extract method (see Extractor_Base_Class.py for detailed documentation)
    def extract(self, text_raw, text_tokenized, pos_list, lemma_list):
        # problem: normalization over # of sentences problematic, as patterns like p.123, cf. xyz fuck up the
        # segmentation. Normalize over number of tokens (separated by whitespaces) instead.
        # Not tragic, as the values will be min-max-normalized at the end
        tokens = len(text_raw.split(" "))
        if tokens == 0:
            return 0
        # first look for [1],[2] etc.
        type_1 = len(self.regex1.findall(text_raw))
        #print(self.regex1.findall(text_raw))
        if type_1>0:
            return type_1/tokens
        # if not succesfull, look for p.123 etc.
        type_2 = len(self.regex2.findall(text_raw))
        #print(self.regex2.findall(text_raw))
        if type_2>0:
            return type_2/tokens
        # if no such citation found, return 0
        return 0

    # override the get_name method (see Extractor_Base_Class.py for detailed documentation)
    def get_name(self):
        return "Citations"







