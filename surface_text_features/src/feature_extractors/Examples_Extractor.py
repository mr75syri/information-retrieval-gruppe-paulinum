# import the base class
from Extractor_Base_Class import Feature_Extractor
import re

""" Feature Extractor to count the example feature
    How many examples does the argument contain
"""


class Examples_Extractor(Feature_Extractor):

    def __init__(self):
        super(Examples_Extractor, self).__init__()
        # TODO: make a separate word list
        self.keywords = ["e.g.", "following examples", "following example", "for example", "an example of this",
                         "for instance", "to give an illustration", "to give an example", "to illustrate", "namely",
                         "this demonstrates", "among other things", "inter alia"]
        self.regex = []
        for keyword in self.keywords:
            self.regex.append(re.compile(keyword))

    def extract(self, text_raw, text_tokenized, pos_list, lemma_list):
        # count the occurrences of examples indicated by predefined keywords
        # most keywords are multi word units so we have to scan the entire sentence instead of single words
        tokens = len(text_raw.split(" "))
        if tokens == 0:
            return 0

        counter = 0
        for expr in self.regex:
            counter += len(expr.findall(text_raw))
        if counter > 0:
            # normalize by the number of tokens
            return counter/tokens
        else:
            return 0

    def get_name(self):
        return "Examples"
