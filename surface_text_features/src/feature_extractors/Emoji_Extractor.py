# import the base class
from Extractor_Base_Class import Feature_Extractor
import re

""" Feature Extractor
   Count the number of emoji in the text
   Maybe more emoji indicate a lower argument quality
"""


class Emoji_Extractor(Feature_Extractor):

    def __init__(self):
        super(Emoji_Extractor, self).__init__()
        self.emoji_list = [r":D[^\w]", ":\)", r"xD", r"\^\^", r"\^\.\^", r":-\)", r";\)", r":3[^\w]", r":o[^\w]", r":O[^\w]", r"=\]", r"=\)", r"8D[^\w]",
                           "XD", r"=D[^\w]", r":\([^\w]", r":'\([^\w]", r":p^[a-z]", r":P^[a-z]", r"<3^[0-9]"]
        self.regex = []
        for emo in self.emoji_list:
            self.regex.append(re.compile(emo))

    def extract(self, text_raw, text_tokenized, pos_list, lemma_list):
        tokens = len(text_raw.split(" "))
        # number of tokens is sometimes larger than raw text separated by whitespaces (return value might be >1)
        # tokens = len(text_tokenized[0])
        # print("tokens", tokens)
        if tokens == 0:
            return 0

        count = 0
        for emo in self.regex:
            count += len(re.findall(emo, text_raw))


        # note: normalize by #tokens or sentence length or...?
        # sentence length might be useless if someone writes "ugly shit"...
        if count > tokens:
            return 1
        return count/tokens

    def get_name(self):
        return "Emoji"