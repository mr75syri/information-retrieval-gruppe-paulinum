# import the base class
from Extractor_Base_Class import Feature_Extractor
from tools.nlp import remove_punctuation_from_tokens

""" Feature_Extractor to extract feature 2 from the wiki 
    inherits Feature_Extractor as every Extractor class must 
"""
class Word_Length_Extractor(Feature_Extractor):

    # override the extract method (see Extractor_Base_Class.py for detailed documentation)
    def extract(self, text_raw, text_tokenized, pos_list, lemma_list):
        # remove punctuation
        tokenized = remove_punctuation_from_tokens(text_tokenized)
        length = 0
        words = 0
        for sent in tokenized:
            for word in sent:
                length+=len(word)
                words+=1
        #print(length, words)
        if words > 0:
            return length/words
        else:
            return 0


    # override the get_name method (see Extractor_Base_Class.py for detailed documentation)
    def get_name(self):
        return "Word_Length"







