# import the base class
from Extractor_Base_Class import Feature_Extractor
from nltk.corpus import stopwords


""" Feature_Extractor to extract feature 11 from the wiki 
    inherits Feature_Extractor as every Extractor class must 
"""
class Non_Stopword_Extractor(Feature_Extractor):

    def __init__(self):
        super(Non_Stopword_Extractor, self).__init__()
        # list of stopwords
        self.stopwords = set(stopwords.words('english'))

    # override the extract method (see Extractor_Base_Class.py for detailed documentation)
    def extract(self, text_raw, text_tokenized, pos_list, lemma_list):
        # count stopwords
        counter = 0
        # normalize over number of tokens
        tokens = 0
        for sent in lemma_list:
            for word in sent:
                if not word.isalnum():
                    continue
                tokens+=1
                if word.lower() in self.stopwords:
                    counter+=1
        if tokens>0:
            return (tokens-counter)/tokens
        else:
            return 0

    # override the get_name method (see Extractor_Base_Class.py for detailed documentation)
    def get_name(self):
        return "Non_Stopwords"







