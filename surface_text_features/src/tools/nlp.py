"""
    module for some auxiliary models that can be used for testing
"""
from nltk import word_tokenize
from nltk import sent_tokenize
from nltk import pos_tag
from nltk.stem import WordNetLemmatizer
from nltk.corpus import wordnet

# lemmatizier is needed in several methods
lemmatizer = WordNetLemmatizer()

# mapping nltk to WordNet-Tags
tag2tag = {
    "J": wordnet.ADJ,
    "V": wordnet.VERB,
    "N": wordnet.NOUN,
    "R": wordnet.ADV
}

# these constants are needed to adress the output of the nlp_pipe function
RAW = "r"
TOKENIZED = "t"
TAGGED = "tg"
LEMMAS = "l"


""" a simple tokenizer that can be used in extract, when text_tokenized is not given 
       used for testing and within the method nlp_pipe
   Args: 
       text(string): the raw text
   Returns: 
       a list of sentences, where each sentence is in turn a list of words, 
           e.g. [['This','is','a','test','sentence','.'],['It','is','used','for','testing','.']] 
           contains punctuation 
   """
def tokenize(text):
    return [word_tokenize(sent) for sent in sent_tokenize(text)]


""" method to remove punctuation from the output of tokenize 
    Args: 
        tokenized: a list of lists as given by tokenize(text) 
    Returns: 
        a list of lists as given by tokenize but without punctuation
"""
def remove_punctuation_from_tokens(tokenized):
    return [[word for word in sent if word.isalpha()] for sent in tokenized]


""" method for POS-Tagging of text 
    only needed for testing
    Args: 
        text: either the raw text or the tokenized text 
        tokenized: indicates wether text is raw or already tokenized 
    Returns: 
        list of list of (word,tag) tuples     
"""
def pos_tagging(text, tokenized = False):
    if not tokenized:
        tokenized = tokenize(text)
    else:
        tokenized = text
    return [pos_tag(sent) for sent in tokenized]


""" method for lemmatization 
    only needed for testing 
    Args: 
        test: raw text 
    Returns: 
        list of list of lemmas
"""
def lemmatize(text):
    preprocessed_text = pos_tagging(tokenize(text), tokenized=True)
    return lemmatize_preprocessed(preprocessed_text)


""" function to lemmatize an already tagged text 
    needed in lemmatize, nlp_pipe
    Arguments: 
        tagged_text: List of list of (word, nltk-Tag) tuples 
    Returns: 
        list of list of lemmas 
"""
def lemmatize_preprocessed(tagged_text):
    # lemmatize a single word
    def lemma(word, tag):
        w_tag = get_word_net_tag(tag)
        if w_tag is None:
            return word
        return lemmatizer.lemmatize(word, pos=w_tag)

    return [[lemma(word, tag) for (word, tag) in sent] for sent in tagged_text]


""" auxiliary function for converting nltk tags to WordNet tags
    Arguments: 
        tag: the nltk-Tag (string) 
    Returns: 
        a wordnet constant (e.g. wordnet.NOUN) if the tag can be mapped to WordNet tags 
            else None 
"""
def get_word_net_tag(tag):
    if tag[0] in tag2tag.keys():
        return tag2tag[tag[0]]
    else:
        return None


""" the whole pipe in one function 
    to be used in the class that calls the feature extractors
    Arguments:
        text: the raw text 
    Returns: 
        a dict as follows, using the constants defined above 
        RAW: the raw text
        TOKENIZED: tokenized text, i.e. list of list of words
        TAGGED: list of list of (word,pos_tag) tuples
        LEMMAS: list of list of lemmas for the tokenized text    
"""
def nlp_pipe(text):
    ret = {}
    ret[RAW] = text
    # tokenization
    tokenized = tokenize(text)
    ret[TOKENIZED] = tokenized
    # pos tagging
    tagged = [pos_tag(sent) for sent in tokenized]
    ret[TAGGED] = tagged
    # lemmatization
    ret[LEMMAS]= lemmatize_preprocessed(tagged)
    return ret
