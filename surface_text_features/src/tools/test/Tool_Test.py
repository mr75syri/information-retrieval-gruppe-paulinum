""" more a sanity check than a test """
from tools.nlp import *

test_sent = "This is a test sentence. It is used to check whether the class 'nlp.py' does what it should."

# check method tokenize
tokenized =  tokenize(test_sent)
print(tokenized)

# check method remove_punctuation_from_tokens
w_o_punctuation = remove_punctuation_from_tokens(tokenized)
print(w_o_punctuation)

# check method pos_tags
pos_tags_not_tokenized = pos_tagging(test_sent)
print(pos_tags_not_tokenized)
pos_tags_already_tokenized = pos_tagging(tokenized, tokenized=True)
print(pos_tags_already_tokenized)

# check method lemmatize
lemmas = lemmatize(test_sent)
print(lemmas)

# test the pipe method
pipe_dict = nlp_pipe(test_sent)
assert(pipe_dict[RAW]==test_sent)
assert(pipe_dict[TOKENIZED]==tokenized)
assert(pipe_dict[TAGGED]==pos_tags_already_tokenized)
assert(pipe_dict[LEMMAS]==lemmas)