"""
the script that calls all the extractors and saves their outputs
first argument must be the file name of the corpus in res (argsme.json typically)
further arguments: names of the extractor classes (without '.py'!!!)
"""

import sys
import pandas as pd
from tools.nlp import  *
from feature_extractors import *
from pydoc import locate
import os
from glob import glob
import pickle

print(sys.argv)
# all given Extractors
extractor_names = sys.argv[2:]

# load their classes using reflection
extractor_classes = [locate("feature_extractors."+extractor_name+"."+extractor_name) for
                     extractor_name in extractor_names]

print(extractor_classes)
# create an instance of every extractor
extractors = [extractor() for extractor in extractor_classes]

# load the data
corpus_name = sys.argv[1]
# load it as a generator that yields (id, text) tuples
# loader = get_loader(corpus_name)
# if loader is None:
#     print("No loader found for "+sys.argv[1])
#     exit(-1)
# arg_generator = loader.generator()
if corpus_name=="args-me":
    files = sorted(glob("../res/nlp_cache/args*.p"))
    #print(files)

elif corpus_name=="dagstuhl":
    files = ["../res/nlp_cache/dagst1.p"]

elif corpus_name == "testargs":
    files = sorted(glob("../res/nlp_cache/testa*.p"))

# initialize checkpoints
# touch cp file
cp_file = "checkpoints/"+sys.argv[1][:5]+"_checkpoints.csv"
with open(cp_file,"a+") as f:
    pass
# initialize the file if necessary
if os.path.getsize(cp_file)==0:
    with open(cp_file, "a+") as f:
        f.writelines("extractor,checkpoint"+os.linesep)

cp = pd.read_csv(cp_file)
print(cp.extractor.tolist())
checkpoints = {}
# keep all the extractor values
for e in cp.extractor.tolist():
    print(e)
    print(cp[cp.extractor==e]["checkpoint"].tolist()[0])
    checkpoints[e]= cp[cp.extractor==e]["checkpoint"].tolist()[0]

for extractor in extractors:
    name= extractor.get_name()
    if(name not in cp.extractor.tolist()):
        #print("name not in cp.extractor")
        checkpoints[name]= 0
    else:
        checkpoints[name]= cp[cp.extractor==name]["checkpoint"][0]
print(checkpoints)
# create files for extractors if necessary
for e_name in [extractor.get_name() for extractor in extractors]:
    if checkpoints[e_name]==0:
        f= open("checkpoints/"+e_name+"_"+sys.argv[1][:5]+".csv", "a+")
        f.close()

# create a checkpoint every...
batch_size = 1000

# dict for the results
results = {}
for e_name in [extractor.get_name() for extractor in extractors]:
    results[e_name] = []

# save the ids of processed arguments in a list (more efficient than dict)
ids = []



# skip first batches if necessary
skip = min(checkpoints.values())
print("Skipping first "+str(skip)+" batch_files")

# now process the corpus
for file_ind in range(len(files[skip:])):
    # create checkpoint
    file = files[skip:][file_ind]
    #print(str(skip*batch_size+counter)+" arguments processed")
    # for every extractor
    for e_name in [extractor.get_name() for extractor in extractors]:
        # new values?
        if len(results[e_name])>0:
            # should be as much new values as args processed since last checkpoint
            assert(len(results[e_name])==len(ids))
            # combine ids with ratings
            data = zip(ids, results[e_name])
            # write back
            with open("checkpoints/"+e_name+"_"+sys.argv[1][:5]+".csv","a+") as fl:
                print("writing ... "+str(file_ind))
                for (id, value) in data:
                    fl.writelines(str(id)+","+str(value)+"\n")
            # update checkpoint value
            checkpoints[e_name]= file_ind
    # update the checkpoint file
    with open(cp_file) as checkpoint_file:
        cp_df = pd.DataFrame.from_dict(checkpoints, orient="index")
        cp_df.index.name = "extractor"
        cp_df.columns = ["checkpoint"]
    cp_df.to_csv(cp_file)
    # reset ids and results
    results = {}
    for e_name in [extractor.get_name() for extractor in extractors]:
        results[e_name] = []
    ids = []

    # load data
    print("loading file "+str(file))
    batch_dict = pickle.load(open(file,"rb"))
    for id in batch_dict.keys():
        # load text
        text_dict = batch_dict[id]
        ids.append(id)


        # extract all features
        for extractor in extractors:
            if file_ind >= checkpoints[extractor.get_name()]:
                value = extractor.extract(text_dict[RAW], text_dict[TOKENIZED], text_dict[TAGGED], text_dict[LEMMAS])
                # save
                results[extractor.get_name()].append(value)

# one last writeback, after finish
for e_name in [extractor.get_name() for extractor in extractors]:
    # new values?
    if len(results[e_name])>0:
        # should be as much new values as args processed since last checkpoint
        assert(len(results[e_name])==len(ids))
        # combine ids with ratings
        data = zip(ids, results[e_name])
        # write back
        with open("checkpoints/"+e_name+"_"+sys.argv[1][:5]+".csv","a+") as file:
            print("writing ... "+str(len(files)))
            for (id, value) in data:
                file.writelines(str(id)+","+str(value)+"\n")
        # update checkpoint value
        checkpoints[e_name]= len(files[skip:])
# update the checkpoint file
with open(cp_file) as checkpoint_file:
    cp_df = pd.DataFrame.from_dict(checkpoints, orient="index")
    cp_df.index.name = "extractor"
    cp_df.columns = ["checkpoint"]
cp_df.to_csv(cp_file)






