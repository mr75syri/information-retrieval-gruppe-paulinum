from abc import ABC, abstractmethod
import os
""" an abstract base class for all corpus loaders (especially one for argsme.json is needed, but 
        maybe later another one for a training corpus)
    all corpus loaders inherit from this class and implement its methods 
"""
class Loader_Base_Class(ABC):

    def __init__(self, file):
        self.file = file
        self.RES_DIR = os.path.abspath("../res/corpora")

    """ method loads the corpus for the script (as a generator in order to prohibit memory errors)
        Arguments:
            filename: the filename of the corpus file in res
        Returns:
            a generator that generates tuples of format id,text, containing all arguments
    """
    @abstractmethod
    def generator(self):
        pass