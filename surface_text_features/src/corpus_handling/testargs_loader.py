from corpus_handling.abstract import Loader_Base_Class
import pandas as pd


""" implements the loader for dagstuhl corpus """
class Testargs_Loader(Loader_Base_Class):

    """ method loads the dagstuhl-corpus for the script
            Arguments:
                filename: the filename of the corpus file in res
            Returns:
                a dict of format id:text, containing all arguments
        """
    def generator(self):
        args= []

        data = pd.read_csv(self.RES_DIR+"/"+self.file)
        data = data[["Premise", "Topic ID", "Argument ID", "Discussion ID"]]
        id_tupels = zip(data["Topic ID"].tolist(), data["Argument ID"].tolist(), data["Discussion ID"].tolist())
        ids = [str(tupel[0])+"-"+str(tupel[1])+"-"+str(tupel[2]) for tupel in id_tupels]
        ls = zip(ids, data["Premise"].tolist())
        ret= {}
        for (id,arg) in ls:
            yield (id,arg)



