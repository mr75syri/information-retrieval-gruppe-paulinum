from corpus_handling.argsme_loader import Argsme_Loader
from corpus_handling.dagstuhl_loader import Dagstuhl_Loader
from corpus_handling.testargs_loader import Testargs_Loader

def get_loader(file):
    if file=="args-me.json":
        return Argsme_Loader(file)
    elif file=="dagstuhl.csv":
        return Dagstuhl_Loader(file)
    elif file=="testargs.csv":
        return Testargs_Loader(file)
    else:
        return None