# sanity check for args_me loader
from corpus_handling.argsme_loader import  Argsme_Loader
generator = Argsme_Loader("args-me.json").generator()
counter = 0
for (id,text) in generator:
    if counter < 5:
        print(id,text)
    else:
        break
    counter+=1
