from corpus_handling.abstract import Loader_Base_Class
import pandas as pd


""" implements the loader for dagstuhl corpus """
class Dagstuhl_Loader(Loader_Base_Class):

    """ method loads the dagstuhl-corpus for the script
            Arguments:
                filename: the filename of the corpus file in res
            Returns:
                a dict of format id:text, containing all arguments
        """
    def generator(self):
        args= []

        data = pd.read_csv(self.RES_DIR+"/"+self.file, encoding="windows-1252")
        data = data[["id","argument"]]
        ls = zip(data.id.tolist(), data.argument.tolist())
        ret= {}
        for (id,arg) in ls:
            yield (id,arg)



