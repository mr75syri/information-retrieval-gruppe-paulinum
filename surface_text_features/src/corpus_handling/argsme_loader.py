from corpus_handling.abstract import Loader_Base_Class
import pandas as pd
import ijson

""" implements the loader for argsme """
class Argsme_Loader(Loader_Base_Class):

    """ method loads the argsme-corpus for the script (as a generator in order to prohibit MemoryErrors)
            Arguments:
                filename: the filename of the corpus file in res
            Returns:
                a tuple of format (id,text), containing all arguments
        """
    def generator(self):
            file = open(self.RES_DIR+"/"+self.file)
            args = ijson.items(file, 'arguments.item')
            for arg in args:
                yield (arg["id"], arg["premises"][0]["text"])
            #print(js)




