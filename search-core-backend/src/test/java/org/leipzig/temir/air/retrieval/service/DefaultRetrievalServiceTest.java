package org.leipzig.temir.air.retrieval.service;

import org.junit.jupiter.api.Test;

class DefaultRetrievalServiceTest {

    @Test
    public void removeStopWordsTest(){
        String test = "This is a Test-String with many Stopwords. Text will be splited into more then one?! now should this also be " +
                      "removed";
        DefaultRetrievalService defaultRetrievalService = new DefaultRetrievalService();
        try {
            System.out.println(defaultRetrievalService.removeStopWords(test));
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

}