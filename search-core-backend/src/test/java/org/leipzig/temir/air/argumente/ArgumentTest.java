package org.leipzig.temir.air.argumente;

import org.apache.lucene.document.Document;
import org.junit.jupiter.api.Test;

class ArgumentTest {

    @Test
    void ArgumenttoDocumentTest() {
        Argument argument = Argument.builder()
                .text("Test test")
                .sourceTitle("Test Source Title")
                .nextArgumentInSourceId("0111")
                .sourceUrl("http://")
                .conclusion("Test conclusion")
                .build();

        Document doc = argument.toDocument();
        assert(doc.getField("conclusion").stringValue().equals("Test conclusion"));
    }
}