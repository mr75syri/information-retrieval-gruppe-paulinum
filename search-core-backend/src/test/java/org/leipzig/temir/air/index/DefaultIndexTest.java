package org.leipzig.temir.air.index;

import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.Query;
import org.junit.Ignore;
import org.junit.jupiter.api.Test;
import org.leipzig.temir.air.argumente.Argument;
import org.leipzig.temir.air.retrieval.query.DefaultQueryInterpreter;
import org.leipzig.temir.air.retrieval.query.QueryInterpreter;
import org.leipzig.temir.air.retrieval.retriever.Retriever;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.List;

@Ignore("Ignoring this test while the buid process is running.")
class DefaultIndexTest {

    @Autowired
    Index index;

    @Autowired
    Retriever retriever;

    @Test
    void basicIndexForAnArgumentWorksTest() {
        Argument argument = Argument.builder()
                .sourceTitle("Test Document")
                .text("Lorem Ipsum ...")
                .conclusion("This is my conclusion")
                .build();

        try {
            index.index(argument.toDocument());
            index.closeIndex();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void doucmentSearchInIndexIsPossible() throws IOException, ParseException {
        String searchPhrase = "my conclusion";
        QueryInterpreter queryInterpreter = new DefaultQueryInterpreter();
        Query query = queryInterpreter.parseQuery(searchPhrase);
        List<Argument> arguments = retriever.retrieve(query,10, null);
        retriever.closeIndex();
    }

}