package org.leipzig.temir.air.retrieval.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.StopFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.DPH;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.util.AttributeFactory;
import org.leipzig.temir.air.argumente.Argument;
import org.leipzig.temir.air.retrieval.query.QueryExpansionService;
import org.leipzig.temir.air.retrieval.query.QueryInterpreter;
import org.leipzig.temir.air.retrieval.retriever.Retriever;
import org.leipzig.temir.air.retrieval.web.ExpansionStrategy;
import org.leipzig.temir.air.retrieval.web.SearchData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.StringReader;
import java.util.*;

@Service
@Slf4j
public class DefaultRetrievalService implements RetrievalService {

    private final List<String> STOP_WORDS = Arrays.asList("i", "me", "my", "myself", "we", "our", "ours", "ourselves", "you", "your",
                                                          "yours", "yourself", "yourselves", "he", "him", "his", "himself", "she", "her",
                                                          "hers", "herself", "it", "its", "itself", "they", "them", "their", "theirs",
                                                          "themselves", "what", "which", "who", "whom", "this", "that", "these", "those",
                                                          "am", "is", "are", "was", "were", "be", "been", "being", "have", "has", "had",
                                                          "having", "do", "does", "did", "doing", "a", "an", "the", "and", "but", "if",
                                                          "or", "because", "as", "until", "while", "of", "at", "by", "for", "with", "about",
                                                          "against", "between", "into", "through", "during", "before", "after", "above",
                                                          "below", "to", "from", "up", "down", "in", "out", "on", "off", "over", "under",
                                                          "again", "further", "then", "once", "here", "there", "when", "where", "why",
                                                          "how", "all", "any", "both", "each", "few", "more", "most", "other", "some",
                                                          "such", "no", "nor", "not", "only", "own", "same", "so", "than", "too", "very",
                                                          "s", "t", "can", "will", "just", "don", "should", "now");

    final CharArraySet STOP_WORDS_CHARSET = new CharArraySet(STOP_WORDS, false);

    //Constants for the score recalculation
    private final float ALPHA = 1;
    private final float BETA  = 1;

    @Autowired
    private Retriever retriever;

    @Autowired
    private QueryInterpreter queryInterpreter;

    @Autowired
    private QueryExpansionService queryExpansionService;

    private List<Argument> getArgumentsOfIndex(String searchPhrase, Integer resultSetSize, Similarity similarity) {
        List<Argument> arguments = null;
        try {
            Query query = queryInterpreter.parseQuery(searchPhrase);
            arguments = retriever.retrieve(query, resultSetSize, similarity);
        }
        catch (ParseException | IOException e) {
            e.printStackTrace();
        }
        return arguments;
    }

    @Override
    public List<Argument> retrieveArguments(SearchData searchData) {
        String query = searchData.getQuery();
        log.info("Searching arguments for query:" + query);

        try {
            query = removeStopWords(query);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        query = this.expandQuery(query, searchData.getExpansionStrategy());
        log.info("Expanded Query:" + query);

        Similarity     similarity = this.getSimilarity(searchData);
        Integer resultSetSizeOfIndex = searchData.getResultSetSize() * 3;
        List<Argument> arguments  = getArgumentsOfIndex(query, resultSetSizeOfIndex, similarity);

        if (searchData.getSortByQuality()) {
            this.rerankWithQualityRating(arguments);
        }

        if (arguments.size() <= searchData.getResultSetSize()){
            return arguments;
        }
        return new ArrayList<>(arguments.subList(0, searchData.getResultSetSize()));
    }

    private Similarity getSimilarity(SearchData searchData) {
        if (searchData.getSimilarity() != null && searchData.getSimilarity()
                                                            .equals("DPH")) {
            return new DPH();
        }
        else {
            return new BM25Similarity();
        }
    }

    private String expandQuery(String query, ExpansionStrategy expansionStrategy) {
        if (expansionStrategy != null) {
            String expandedQuery = queryExpansionService.expandQuery(query, expansionStrategy);
            if (expandedQuery == null) {
                return query;
            }
            else {
                return expandedQuery;
            }
        }
        else {
            return query;
        }

    }

    /**
     * Sorting arguments by the quality rating
     *
     * @param arguments sorted arguments
     */
    private void rerankWithQualityRating(List<Argument> arguments) {
        float maxScore = arguments.get(0)
                                  .getScore();
        arguments.sort(Comparator.comparing(Argument::getRating));
        Collections.reverse(arguments);
        float maxRating = arguments.get(0)
                                   .getRating();
        for (Argument argument : arguments) {
            argument.setScore(calculateScore(argument, maxScore, maxRating));
        }
        arguments.sort(Comparator.comparing(Argument::getScore));
        Collections.reverse(arguments);
    }

    /**
     * Calculating the score of an argument with use of quality ratings
     * score = alpha * score_normalized + beta * rating_normalized
     *
     * @param argument
     *
     * @return
     */
    private Float calculateScore(Argument argument, Float maxScore, Float maxRating) {
        Float defaultScore  = argument.getScore();
        Float defaultRating = argument.getRating();
        return ALPHA * normalize(0, maxScore, defaultScore) * BETA * normalize(0, maxRating, defaultRating);
    }

    /**
     * Calculation of the Z-Normalisation
     *
     * @param min
     * @param max
     * @param x
     *
     * @return
     */
    private float normalize(float min, float max, float x) {
        return (x - min) / (max - min);
    }

    public String removeStopWords(String text) throws IOException {
        StandardTokenizer tokenizer = new StandardTokenizer(AttributeFactory.DEFAULT_ATTRIBUTE_FACTORY);
        tokenizer.setReader(new StringReader(text));
        TokenStream tokenStream = tokenizer;
        tokenStream = new StopFilter(tokenStream, this.STOP_WORDS_CHARSET);
        StringBuilder     sb                = new StringBuilder();
        CharTermAttribute charTermAttribute = tokenStream.addAttribute(CharTermAttribute.class);
        tokenStream.reset();
        while (tokenStream.incrementToken()) {
            String term = charTermAttribute.toString();
            sb.append(term)
              .append(" ");
        }
        return sb.toString().trim();
    }
}
