package org.leipzig.temir.air.configuration;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Getter
@AllArgsConstructor
public class GlobalConfigurationService {
    private String queryExpansionHost;
}
