package org.leipzig.temir.air.retrieval.query;

import org.leipzig.temir.air.retrieval.web.ExpansionStrategy;

public interface QueryExpansionService {

    /**
     * Expand given query with the ExpansionStrategy
     * @param query
     * @return
     */
    String expandQuery(String query, ExpansionStrategy expansionStrategy);
}
