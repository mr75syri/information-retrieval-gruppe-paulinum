package org.leipzig.temir.air.index;

import org.apache.lucene.document.Document;
import org.leipzig.temir.air.argumente.Argument;

import java.io.IOException;
import java.util.stream.Stream;

/**
 * Basic interface store and index documents in the index.
 * The Analyzer for the indexing process will configure here.
 */
public interface Index {

    /**
     * Start indexing for a single document
     *
     * @param document document
     */
    void index(Document document) throws IOException;

    /**
     * Indexes all arguments from the stream.
     *
     * @param arguments The stream of arguments
     */
    void indexAll(final Stream<Argument> arguments);


    /**
     * Closes the index.
     * <p>
     * This method may perform some clean-up or finalizing steps.
     * </p>
     */
    void closeIndex() throws IOException;

}
