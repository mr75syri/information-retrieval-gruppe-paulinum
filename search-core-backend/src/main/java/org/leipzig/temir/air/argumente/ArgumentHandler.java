package org.leipzig.temir.air.argumente;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class ArgumentHandler {
    private List<Argument> argumentList;
}
