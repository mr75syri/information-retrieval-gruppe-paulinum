package org.leipzig.temir.air.index.batch;

import org.apache.lucene.document.Document;
import org.leipzig.temir.air.argumente.Argument;
import org.springframework.batch.item.ItemProcessor;


public class ArgumentItemProcessor implements ItemProcessor<Argument, Document> {


    @Override
    public Document process(Argument argument) {
        return argument.toDocument();
    }
}
