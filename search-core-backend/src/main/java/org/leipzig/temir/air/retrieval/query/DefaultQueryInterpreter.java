package org.leipzig.temir.air.retrieval.query;


import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.Query;
import org.leipzig.temir.air.analyzer.AnalyzerFactory;
import org.leipzig.temir.air.argumente.annotations.DefaultAnnotationProcessor;
import org.springframework.stereotype.Component;

@Component
public class DefaultQueryInterpreter implements QueryInterpreter {

    @Override
    public Query parseQuery(String searchPhrase) throws ParseException {
        String[] fields = DefaultAnnotationProcessor.getSearchableFields().toArray(new String[0]);
        MultiFieldQueryParser queryParser = new MultiFieldQueryParser(fields, AnalyzerFactory.getAnalyzer());
        return queryParser.parse(searchPhrase);
    }
}
