package org.leipzig.temir.air.index.batch;

import org.apache.lucene.document.Document;
import org.leipzig.temir.air.index.Index;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.List;

public class DocumentItemWriter implements ItemWriter<Document> {

    @Autowired
    Index index;

    @Override
    public void write(List<? extends Document> list) throws Exception {
        for(Document el : list){
            try {
                index.index(el);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        index.closeIndex();
    }
}
