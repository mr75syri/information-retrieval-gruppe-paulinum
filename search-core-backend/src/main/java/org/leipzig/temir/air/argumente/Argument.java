package org.leipzig.temir.air.argumente;

import lombok.*;
import org.apache.lucene.document.Document;
import org.leipzig.temir.air.argumente.annotations.DefaultAnnotationProcessor;
import org.leipzig.temir.air.argumente.annotations.LuceneIndexed;
import org.leipzig.temir.air.argumente.annotations.LuceneSearchable;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Argument {
    @LuceneIndexed(fieldType = "STRING")
    @LuceneSearchable
    String id;

    @LuceneIndexed(fieldType = "TEXT")
    @LuceneSearchable
    String conclusion;

    @LuceneIndexed(fieldType = "TEXT")
    @LuceneSearchable
    String text;

    @LuceneIndexed(fieldType = "STRING")
    String stance;

    @LuceneIndexed(fieldType = "STRING")
    String sourceId;

    @LuceneIndexed(fieldType = "STRING")
    String previousArgumentSourceId;
    String acquisitionTime;

    @LuceneIndexed(fieldType = "STRING")
    @LuceneSearchable
    String discussionTitle;

    @LuceneIndexed(fieldType = "STRING")
    String sourceTitle;

    @LuceneIndexed(fieldType = "STRING")
    String sourceUrl;

    @LuceneIndexed(fieldType = "STRING")
    String nextArgumentInSourceId;

    @LuceneIndexed(fieldType = "FLOAT")
    @LuceneSearchable
    Float rating;

    //This field is necessary for the output represantation
    Float score;

    /**
     * Parsing the Argument to a Lucene Document by using the AnnotationProcessor needed for the indexing. All attributes
     * which are mark with @LuceneIndexed will be added to the Lucene Document.
     *
     * @return Document document
     */
    public Document toDocument() {
        Document document = new Document();
        try {
            DefaultAnnotationProcessor.getFieldsToIndex(this).forEach(document::add);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return document;
    }

    /**
     * Creating are Argument based on a Lucene Object which is typical retrieved by the index through the index process
     *
     * @param document Document
     * @return Argument
     */
    public static Argument of(Document document) {
        try {
            return DefaultAnnotationProcessor.DocumentToArgument(document);
        } catch (IllegalAccessException | InstantiationException e) {
            System.err.println(String.format("Error while initialize a Argument of Document %s", document.toString()));
            return null;
        }
    }

    /**
     * Creating are Argument based on a Lucene Object which is typical retrieved by the index through the index process
     * Including the score value of the search result.
     * @param document Document
     * @return Argument
     */
    public static Argument of(Document document, Float score) {
        try {
            Argument argument = DefaultAnnotationProcessor.DocumentToArgument(document);
            argument.setScore(score);
            return argument;
        } catch (IllegalAccessException | InstantiationException e) {
            System.err.println(String.format("Error while initialize a Argument of Document %s", document.toString()));
            return null;
        }
    }
}
