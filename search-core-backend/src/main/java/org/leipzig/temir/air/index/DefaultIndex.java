package org.leipzig.temir.air.index;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.leipzig.temir.air.analyzer.AnalyzerFactory;
import org.leipzig.temir.air.argumente.Argument;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.stream.Stream;

@Component
public class DefaultIndex implements Index {

    @Value("${indexlocation}")
    private String indexLocation;

    private IndexWriter indexWriter;

    public DefaultIndex(@Value("${indexlocation}") String indexLocation) {
        try {
            this.openIndex(indexLocation);
        }
        catch (IOException e) {
            throw new BeanCreationException(
                    String.format("Initialization error for %s. Can not open index at %s", this.getClass(), indexLocation));
        }
    }

    @Override
    public void index(Document document) throws IOException {
        if(indexWriter.isOpen()){
            indexWriter.addDocument(document);
        } else {
            this.openIndex(this.indexLocation);
        }

    }

    @Override
    public void indexAll(Stream<Argument> arguments) {

    }

    private void openIndex(String indexLocation) throws IOException {
        final File indexDirectory = new File(indexLocation);
        if (!indexDirectory.exists()) {
            indexDirectory.mkdirs();
        }
        Directory         directory     = FSDirectory.open(indexDirectory.toPath());
        IndexWriterConfig configuration = this.createConfiguration();
        this.indexWriter = new IndexWriter(directory, configuration);
    }

    @Override
    public void closeIndex() throws IOException {
        indexWriter.commit();
        indexWriter.close();
    }

    private IndexWriterConfig createConfiguration() {
        Analyzer analyzer = AnalyzerFactory.getAnalyzer();
        return new IndexWriterConfig(analyzer);
    }
}
