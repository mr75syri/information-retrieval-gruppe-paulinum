package org.leipzig.temir.air.retrieval.web;

public enum ExpansionStrategy {
    NONE, BASELINE, DMC, NLTM
}
