package org.leipzig.temir.air.argumente.annotations;

import org.apache.lucene.document.*;
import org.leipzig.temir.air.argumente.Argument;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class DefaultAnnotationProcessor {

    /**
     * Return a list of Lucene Fields. Field will be founded by annotated class attributes.
     * @param object object
     * @return fieldsToIndex
     * @throws IllegalAccessException
     */
    public static List<org.apache.lucene.document.Field> getFieldsToIndex(Object object) throws IllegalAccessException {
        List<org.apache.lucene.document.Field> fieldsToIndex = new ArrayList<>();
        Class<?> clazz = object.getClass();
        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(LuceneIndexed.class)) {
                Annotation annotation = field.getAnnotation(LuceneIndexed.class);
                LuceneIndexed luceneIndexed = (LuceneIndexed) annotation;
                switch (luceneIndexed.fieldType()) {
                    case "TEXT":
                        if(!Objects.isNull(field.get(object))) {
                            fieldsToIndex.add(new TextField(field.getName(), (String) field.get(object), luceneIndexed.store()));
                        }
                        break;
                    case "STRING":
                        if(!Objects.isNull(field.get(object))){
                            fieldsToIndex.add(new StringField(field.getName(), (String) field.get(object), luceneIndexed.store()));
                        }
                        break;
                    case "STANCE":
                        if(!Objects.isNull(field.get(object))) {
                            fieldsToIndex.add(new StringField(field.getName(), field.get(object).toString(), luceneIndexed.store()));
                        }
                        break;
                    case "INTEGER":
                        fieldsToIndex.add(new IntPoint(field.getName(), (Integer) field.get(object)));
                        break;
                    case "FLOAT":
                        fieldsToIndex.add(new StoredField(field.getName(), (Float) field.get(object)));
                        break;
                }
            }
        }
        return fieldsToIndex;
    }

    /**
     * Return a list of names of the searchable field of a class. This Fields are annotated with @Searchable
     * @return searchableFieldNames
     */
    public static List<String> getSearchableFields() {
        List<String> searchableFieldNames = new ArrayList<>();
        Class<?> clazz = Argument.class;
        for (Field field : clazz.getDeclaredFields()) {
            if (field.isAnnotationPresent(LuceneSearchable.class)) {
                searchableFieldNames.add(field.getName());
            }
        }
        return searchableFieldNames;
    }

    /**
     * Parsing a given Lucene document to an Argument object by using the reflection api.
     * @param document
     * @return
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    public static Argument DocumentToArgument(Document document) throws IllegalAccessException, InstantiationException {
        Class<?> clazz = Argument.class;
        Object argument = clazz.newInstance();
        document.getFields().forEach(field -> {
            try {
                //Field name should be the name of the attribute in the argument class
                Field attributeField = argument.getClass().getDeclaredField(field.name());
                attributeField.setAccessible(true);
                String attributeValue = field.stringValue();
                switch (attributeField.getType().getName()){
                    case "java.lang.String":
                        attributeField.set(argument, attributeValue);
                        break;
                    case "java.lang.Integer":
                        attributeField.set(argument, Integer.parseInt(attributeValue));
                        break;
                    case "java.lang.Float":
                        attributeField.set(argument, Float.parseFloat(attributeValue));
                        break;
                }
            } catch (NoSuchFieldException | IllegalAccessException e) {
                e.printStackTrace();
            }
        });
        return (Argument) argument;
    }
}
