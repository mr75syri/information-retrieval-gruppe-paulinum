package org.leipzig.temir.air.retrieval.web;

import lombok.Getter;
import lombok.Value;

/**
 * POJO class for the payload of the post request
 */
@Getter
@Value
public class SearchData {
    private String query;
    private Integer resultSetSize;
    private Boolean sortByQuality;
    private ExpansionStrategy expansionStrategy;
    private String similarity;

    public Integer getResultSetSize(){
        if(this.resultSetSize == null){
            return 10;
        } else {
            return this.resultSetSize;
        }
    }
}
