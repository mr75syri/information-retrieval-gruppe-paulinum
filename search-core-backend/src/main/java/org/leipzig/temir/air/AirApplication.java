package org.leipzig.temir.air;

import org.leipzig.temir.air.configuration.GlobalConfigurationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class AirApplication {

    public static void main(String[] args) {
        SpringApplication.run(AirApplication.class, args);
    }

    @Bean
    public GlobalConfigurationService globalConfigurationService(
            @Value("${queryExpansionHost}") String argumentValue) {
        System.out.println(">>>> Set query expansion host to : " + argumentValue);
        return new GlobalConfigurationService(argumentValue);
    }

}
