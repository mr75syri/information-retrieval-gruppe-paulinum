package org.leipzig.temir.air.index.batch;

import org.apache.lucene.document.Document;
import org.leipzig.temir.air.argumente.Argument;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

    @Value("${path_to_import_file}")
    private String PATH_TO_IMPORT_FILE;

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    /**
     * ItemReader: read the elements of the large json array and transform this values into arguments
     * @return
     */
    @Bean
    public FlatFileItemReader<Argument> reader() {
        Resource importData = new FileSystemResource(PATH_TO_IMPORT_FILE);
        return new FlatFileItemReaderBuilder<Argument>()
                .name("argumentItemReader")
                .resource(importData)
                .delimited()
                .names("id","text","stance","sourceId","previousArgumentInSourceId","acquisitionTime","discussionTitle","sourceTitle",
                       "sourceUrl","nextArgumentInSourceId","conclusion", "rating")
                .fieldSetMapper(new BeanWrapperFieldSetMapper<Argument>() {{
                    setTargetType(Argument.class);
                }})
                .strict(false)
                .build();
    }

    @Bean
    public ArgumentItemProcessor processor() {
        return new ArgumentItemProcessor();
    }

    @Bean
    public ItemWriter<Document> writer(){
        return new DocumentItemWriter();
    }

    @Bean
    public Job indexArguments(Step step1) {
        return jobBuilderFactory.get("indexArguments")
                .incrementer(new RunIdIncrementer())
                .flow(step1)
                .end()
                .build();
    }

    @Bean
    public Step step1(DocumentItemWriter writer) {
        return stepBuilderFactory.get("step1")
                .<Argument, Document> chunk(10000)
                .reader(reader())
                .processor(processor())
                .writer(writer)
                .build();
    }
}
