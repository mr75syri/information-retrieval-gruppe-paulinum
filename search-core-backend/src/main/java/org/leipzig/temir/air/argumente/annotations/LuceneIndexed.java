package org.leipzig.temir.air.argumente.annotations;

import org.apache.lucene.document.Field;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface LuceneIndexed {
    String fieldType() default "";
    Field.Store store() default Field.Store.YES;
}
