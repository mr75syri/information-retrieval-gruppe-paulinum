package org.leipzig.temir.air.retrieval.query;

import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.Query;

/**
 * First step of the search. The Query will be consumed by the QueryInterpreter. The output is a query which lucene
 * understand for the search process.
 * Additional query expansion could included too.
 */
public interface QueryInterpreter {

    /**
     * Parsing a search phrase to a query, needed for search in the index
     * @param searchPhrase
     * @return
     */
    Query parseQuery(String searchPhrase) throws ParseException;
}
