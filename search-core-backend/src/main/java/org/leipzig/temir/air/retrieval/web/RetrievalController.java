package org.leipzig.temir.air.retrieval.web;

import org.leipzig.temir.air.argumente.Argument;
import org.leipzig.temir.air.retrieval.service.RetrievalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/search")
@CrossOrigin
public class RetrievalController {

    @Autowired
    RetrievalService retrievalService;

    @PostMapping
    public ResponseEntity<List<Argument>> getArguments(@RequestBody SearchData searchData){
        return ResponseEntity.of(Optional.of(this.retrievalService.retrieveArguments(searchData)));
    }
}
