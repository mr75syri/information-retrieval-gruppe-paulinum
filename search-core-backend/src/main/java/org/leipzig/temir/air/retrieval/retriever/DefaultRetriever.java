package org.leipzig.temir.air.retrieval.retriever;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.DPH;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.FSDirectory;
import org.leipzig.temir.air.argumente.Argument;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Component
public class DefaultRetriever implements Retriever {

    IndexReader indexReader;
    IndexSearcher indexSearcher;
    String indexLocation;

    public DefaultRetriever(@Value("${indexlocation}") String indexLocation) {
        this.indexLocation = indexLocation;
        final FSDirectory directory;
        try {
            directory = FSDirectory.open(Paths.get(indexLocation));
            this.indexReader = DirectoryReader.open(directory);
            this.indexSearcher = new IndexSearcher(this.indexReader);
        }
        catch (IOException e) {
            System.out.println(
                    format("%s can not be initialized cause index path (%s) not exit", this.getClass(), indexLocation));
        }

    }

    @Override
    public void openIndex(String indexLocation) throws IOException {
        final FSDirectory directory = FSDirectory.open(Paths.get(indexLocation));
        this.indexReader = DirectoryReader.open(directory);
        this.indexSearcher = new IndexSearcher(this.indexReader);
    }

    @Override
    public void closeIndex() throws IOException {
        if (this.indexReader != null) {
            this.indexReader.close();
            this.indexReader = null;
        }
    }

    /**
     * Retrieve documents of the index.
     *
     * @param interpretedQuery interpreted Query
     *
     * @return arguments list of arguments
     *
     * @throws IOException
     */

    @Override
    public List<Argument> retrieve(Query interpretedQuery, Integer resultSetSize, Similarity similarity) throws IOException {
        if(indexSearcher == null){
            this.openIndex(this.indexLocation);
        }
        this.indexSearcher.setSimilarity(similarity);
        TopDocs topDocs = indexSearcher.search(interpretedQuery, resultSetSize);
        return Arrays.stream(topDocs.scoreDocs)
                     .map(doc -> new Object[] {getDocumentOfIndex(doc.doc), doc.score})
                     .map(obj -> Argument.of((Document) obj[0], (Float) obj[1]))
                     .filter(distinctByKey(Argument::getId))
                     .collect(Collectors.toList());
    }

    private Document getDocumentOfIndex(int documentId) {
        try {
            return indexSearcher.doc(documentId);
        }
        catch (IOException e) {
            return null;
        }
    }

    private static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {

        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

}
