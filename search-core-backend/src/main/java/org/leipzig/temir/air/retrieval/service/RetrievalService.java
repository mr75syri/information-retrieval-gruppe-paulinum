package org.leipzig.temir.air.retrieval.service;

import org.apache.lucene.search.similarities.Similarity;
import org.leipzig.temir.air.argumente.Argument;
import org.leipzig.temir.air.retrieval.web.SearchData;

import java.util.List;

/**
 * Service which is used by the controller to retrieval arguments of the index.
 */
public interface RetrievalService {

    List<Argument> retrieveArguments(SearchData searchData);
}
