package org.leipzig.temir.air.retrieval.query;

import org.leipzig.temir.air.configuration.GlobalConfigurationService;
import org.leipzig.temir.air.retrieval.web.ExpansionStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Service
public class DefaultQueryExpansionService implements QueryExpansionService {

    @Value("${expansionServiceBaseUrl}")
    private String expansionServiceBaseUrl;

    @Autowired
    private GlobalConfigurationService globalConfigurationService;

    @Override
    public String expandQuery(String query, ExpansionStrategy expansionStrategy) {
        switch (expansionStrategy){
            case BASELINE:
                return this.expandBaseline(query);
            case DMC:
                return this.expandDmc(query);
            case NLTM:
                return this.expandNltm(query);
            default:
                return query;
        }
    }

    private String buildUrl(){
        return globalConfigurationService.getQueryExpansionHost() + expansionServiceBaseUrl;
    }

    private String expandDmc(String query) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        String requestJson = "{\"query\": \"" + query + "\"}";
        String url = buildUrl() + "dmc";
        HttpEntity<String> entity = new HttpEntity<String>(requestJson, headers);
        ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);
        return response.getBody();
    }

    private String expandBaseline(String query) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        String requestJson = "{\"query\": \"" + query + "\"}";
        String url = buildUrl() + "baseline";
        HttpEntity<String> entity = new HttpEntity<String>(requestJson, headers);
        ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);
        return response.getBody();
    }

    private String expandNltm(String query) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        String requestJson = "{\"query\": \"" + query + "\"}";
        String url = buildUrl() + "ntlm";
        HttpEntity<String> entity = new HttpEntity<String>(requestJson, headers);
        ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);
        return response.getBody();
    }
}
