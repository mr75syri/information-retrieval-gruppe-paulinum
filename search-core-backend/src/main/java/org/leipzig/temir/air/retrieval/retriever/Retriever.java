package org.leipzig.temir.air.retrieval.retriever;

import org.apache.lucene.search.Query;
import org.apache.lucene.search.similarities.Similarity;
import org.leipzig.temir.air.argumente.Argument;

import java.io.IOException;
import java.util.List;

/**
 * Basic functionality to retrieve documents of the index.
 */
public interface Retriever {

    /**
     * Open index at location
     * @param indexLocation index location
     */
    void openIndex(String indexLocation) throws IOException;

    /**
     * close index
     */
    void closeIndex() throws IOException;

    List<Argument> retrieve(Query interpretedQuery, Integer resultSetSize, Similarity similarity) throws IOException;
}
