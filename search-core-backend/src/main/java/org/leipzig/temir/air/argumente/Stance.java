package org.leipzig.temir.air.argumente;

public enum Stance {
    PRO {
        @Override
        public String toString(){
            return "PRO";
        }
    }, CON {
        @Override
        public String toString(){
            return "CON";
        }
    }
}
