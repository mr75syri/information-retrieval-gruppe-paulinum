# Query Expansion 

* <b>res</b>: für Term-Document-Matrix, Embeddings etc.  
	- <b> embeddings.bin </b>: word2vec-Embeddings für lemmatisierten Korpus. [zu groß, Download über Google Drive](https://drive.google.com/open?id=1wBJC06nDukT8I3td258DtyCe7w0q6X-L) 
	- <b> tdmatrix.npz </b>: Term-Document-Matrix über lemmatisierten Korpus. [zu groß, Download über Google Drive](https://drive.google.com/open?id=1JUU8TJG5_eNFdYe5XE9CDcxbCSXirOEB) 
	- <b> arg2id.p <b>, <b> word2id.p</b>: (werden für Term-Document-Matrix benötigt)  
	- <b> d600_matrix.npy </b>: für die neuen Embeddings [zu groß, Download über Google Drive](https://drive.google.com/open?id=1qRtw-d5rT9VE5YZ9nl43DXabp2zUaOPF) 
	- <b< dim_600_ind2word.p, dim_600_word2ind.p </b> werden für die neuen Embeddings benötigt
* <b> src/embeddings </b>: enthält die Klassen, die Schnittstellen zu Embeddings anbieten. Diese Klassen sollen alle die abstract.Embeddings implementieren. 
* <b> src/expansors </b>: enthält für jeden Expansion-Ansatz eine Klasse, die die Methode expand der Klasse abstract.Expansor implementiert 
* <b> src/tools </b>: was sonst so benötigt wird, bis jetzt: 
	- <b> query.py </b> mit util-Methoden für das Verarbeiten der Query: Lemmatizer (nicht sicher, ob benötigt); Methode, die Query im Lucene-Format zusammenbaut 
	- <b> tdmatrix.py </b> als Schnittstelle zur Term-Document-Matrix (wird in zumindest einem der Ansätze benötigt)