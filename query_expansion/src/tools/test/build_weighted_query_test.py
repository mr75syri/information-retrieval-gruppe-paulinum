from tools.query import build_weighted_query

# some well-formed inputs
inputs = [
    {"word1":0.25, "word2":1.0, "word3":0.33},
    {"word":1.0},
    [("word1",0.22),("word2",0.55),("word4",0.75),("word5",0.8)],
    [("word4",0.0)]
]

for inp in inputs:
    print(build_weighted_query(inp))

# tuples with word on second position

inputs2 = [
    [(0.55,"word1"), (0.22, "word2")],
    [(0.33,"word1")]
]

for inp in inputs2:
    print(build_weighted_query(inp, weight_first=True))

# some malformed ones
malformed = [
    "test",
    [("word1",0.43),"test"],
    [("word1"),0.43]
]

for inp in malformed:
    try:
        print(build_weighted_query(inp))
    except:
        print("Malformed Input correctly recognized!")