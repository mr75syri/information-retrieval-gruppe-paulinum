from tools.tdmatrix import TDMatrix

matrix = TDMatrix()

# tf in argument1
arg_id = 'c67482ba-2019-04-18T13:32:05Z-00000-000'

assert(matrix.tf("opponent",arg_id)==1)

assert(matrix.tf("test", arg_id)==0)

# problem: hard to check wether correct
print(matrix.idf("opponent"))
print(matrix.idf("the"))

# not in vocab
try:
    matrix.tf("alvhsdfogavbx,mcb")
except:
    print("Out of vocab correctly recognized")

try:
    matrix.idf("alvhsdfogavbx,mcb")
except:
    print("Out of vocab correctly recognized")

