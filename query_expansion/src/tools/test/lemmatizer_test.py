from tools.query import Lemmatizer

lemmatizer = Lemmatizer()

# some sample queries
queries = [
    "this is an interesting test",
    "I'd like to see some arguments",
    "a very nice query",
    "",
    "adfhlvnlaehrafbslakjfhajkls"
]

for query in queries:
    print()
    print(query)
    print("Lemmatized:")
    lemmas = lemmatizer.lemmatize(query)
    print(lemmatizer.lemmatize(query))
