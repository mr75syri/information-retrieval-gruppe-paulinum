""" some utility functions for processing a query"""
from nltk import word_tokenize
from nltk import sent_tokenize
from nltk import pos_tag
from nltk.stem import WordNetLemmatizer
from nltk.corpus import wordnet


class Lemmatizer:
    """ class for lemmatizing (class s.t. lemmatizer is only initialised once)
    call lemmatize(query) on Lemmatizer object, all other methods only needed internally
    """

    def __init__(self):
        self.__lemmatizer = WordNetLemmatizer()

        # map nltk to wordnet-tags
        self.__tag2tag = {
            "J": wordnet.ADJ,
            "V": wordnet.VERB,
            "N": wordnet.NOUN,
            "R": wordnet.ADV
        }



    def lemmatize(self, query):
        """
        method to lemmatize a given query
        :param query: the query, as it is issued by the user
        :type query: string
        :returns: the lemmatized query
        :rtype: string
        """
        tokens = [word_tokenize(sent) for sent in sent_tokenize(query)]
        tagged = [pos_tag(token) for token in tokens]
        lemmas = []
        for sent in tagged:
            for (word,tag) in sent:
                lemmas.append(self.__lemma(word, tag))
        return lemmas


    def __get_word_net_tag(self, tag):
        """
        auxiliary method to do the mapping from nltk to wordnet tags
        :param tag: the tag to map
        :return: the mapped tag
        """
        if tag[0] in self.__tag2tag.keys():
            return self.__tag2tag[tag[0]]
        else:
            return None

    def __lemma(self, word, tag):
        """
        lemmatization of one word, only needed internally
        :param word: the word to lemmatize
        :param tag: its tag
        :return: the lemmatized word
        """
        w_tag = self.__get_word_net_tag(tag)
        if w_tag is None:
            return word
        return self.__lemmatizer.lemmatize(word, pos=w_tag)


def build_weighted_query(weights, weight_first=False):
    """
    builds a weighted query in lucene format from given word-weight pairs
    :param weights: a dictionary with words as keys and their respective weights as values or
            a list of (word, weight) tuples or a list of (weight, word) tuples (in this case, set weight_first to True)
    :type weights: dictionary or list of (word,weight) tuples or list of (weight, word) tuples
    :param weight_first: set to True, if list of (weight,word) tuples given
    :returns: a weighted query in lucene format
    :rtype: string
    """
    query = ""
    # is it a dict
    if isinstance(weights, dict):
        for word in weights.keys():
            query = query+str(word)+"^"
            query = query+str(weights[word])+" "
    # or a list?
    elif isinstance(weights, list):
        if not weight_first:
            # tuples with word first
            for tpl in weights:
                if not isinstance(tpl, tuple):
                    raise AttributeError("Input malformed")
                else:
                    query = query+str(tpl[0])+"^"+str(tpl[1])+" "
        else:
            # tuples with weight first
            for tpl in weights:
                if not isinstance(tpl, tuple):
                    raise AttributeError("Input malformed")
                else:
                    query = query+str(tpl[1])+"^"+str(tpl[0])+" "
    # neiter a dict nor a tuple
    else:
        raise AttributeError("Input malformed")

    return query.strip()
