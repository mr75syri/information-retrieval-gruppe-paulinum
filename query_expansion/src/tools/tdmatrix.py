import os
import pickle
from numpy import load
from scipy.sparse import csr_matrix
import numpy as np

from definitions import RES_DIR


class TDMatrix:
    """ class for word statistics, especially tf, idf etc. based on a term-document matrix"""

    def __init__(self, matrix_file="tdmatrix.npz", word2id_file="word2id.p", arg2id_file="arg2id.p", idf_file="idf.p"):
        """
        :param matrix_file: the filepath to the npz file containing the precomputed matrix
        :param word2id_file: the filepath to the pickle file containing the mappings from words to their ids
        :param arg2id_file: the filepath to the pickle file containing the mappings from args-me-ids to ids in the matrix
        """
        # load arg2id and word2id
        self.arg2id = pickle.load(open(RES_DIR+os.sep+arg2id_file, "rb"))
        self.word2id = pickle.load(open(RES_DIR + os.sep + word2id_file, "rb"))
        # load the matrix
        matrix_path = RES_DIR+os.sep+matrix_file
        with load(matrix_path) as files:
            data = files['data']
            form = files['format']
            indices = files['indices']
            indptr = files['indptr']
            shape = files['shape']
        self.matrix = csr_matrix((data, indices, indptr), shape=shape)
        self.__idf = pickle.load(open(RES_DIR + os.sep + idf_file, "rb"))





    def tf(self, word, document):
        """ computes the absolute frequency of word in document

        :param word: a word as string
        :param document: argsme-ID as string
        :returns: absolute frequency of word in document, None if no valid document id
        :rtype: int or None
        """
        if document not in self.arg2id.keys():
            raise AttributeError("no valid argument id")
        return self.matrix[self.arg2id[document], self.word2id[word]]

    def idf(self, word):
        """ computes idf for a word

        :param word: word as a string
        :returns absolute frequency of documents containing the word
        :rtype: int
        """
        if word not in self.word2id.keys():
            raise AttributeError("word not in vocabulary")
        word_id = self.word2id[word]
        return self.__idf[word_id]

    def contains_word(self, word):
        return word.strip() in self.word2id.keys()

    def get_doc_col(self, document):
        print(self.matrix.shape)
        """
        returns the row of the matrix for a given doc

        :param document: the argsme-Argument-ID as a string
        :returns matrix row
        :rtype np-array
        """
        if document not in self.arg2id.keys():
            raise AttributeError("no valid argument id")
        return self.matrix[self.arg2id[document]].todense()





