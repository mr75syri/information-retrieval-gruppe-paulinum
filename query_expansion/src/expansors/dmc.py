from expansors.abstract import Expansor
import numpy as np
from tools.query import build_weighted_query

class DMCExpansor(Expansor):
    """
    implements the expansor proposed by Diaz, Mitra and Craswell (paper 12 in the wiki)
    """


    def __init__(self, embeddings, lmbda=0.1, keep_originals=True, threshold=10):
        super(DMCExpansor, self).__init__(embeddings, keep_originals, threshold)
        # the lambda parameter
        self.lmbda = lmbda



    def expand(self, query, k=10):
        # split query in tokens
        q = query.split(" ")
        # eliminate tokens that are not in vocab
        q_clean = [word for word in q if not (self.embeddings.get_word_id(word) is None)]
        # nothing to expand?
        if len(q_clean) == 0:
            return ""
        q = q_clean
        # get ids of tokens
        q_ids = [self.embeddings.get_word_id(q[i]) for i in range(len(q))]
        # now determine the relevant columns of U*U^T
        # only the columns corresponding to word ids in q need to be non-zero
        # that is, multiply every row in U with the word-id columns in U^T
        U = self.embeddings.get_matrix()
        # the results
        word_vec = []
        for i in range(len(U)):
            # the result for word i
            res = 0
            row = U[i]
            for q_id in q_ids:
                column = U.T[:,q_id]
                # row x column
                prod = row.dot(column)
                # does the final multiplication with vector q
                res+= prod
            word_vec.append(res)
        # now pair with the ids
        result_vec = [(word_vec[i], i) for i in range(len(word_vec))]
        result_vec = sorted(result_vec, key=lambda x:x[0], reverse=True)
        # incrementally fetch top-k *|q|
        ind = 0
        top_k = list()
        # print("now fetching "+str(k*len(q))+" words")
        ind = 0
        # go through the results...
        while len(top_k)<k+len(q):
            # looked at all the words (shouldn't happen in a meaningful query)
            if ind == len(result_vec):
                break

            word = self.embeddings.id_2_word(result_vec[ind][1])
            # only add word to expansion if in corpus vocab and its idf is above the threshold
            if (not self.tdmatrix.contains_word(word)) or self.tdmatrix.idf(word)<self.threshold:
                ind+=1
                continue
            weight = result_vec[ind][0]
            # do the weightings
            wght = self.lmbda*(1/len(q)) + ((1-self.lmbda)*weight)
            top_k+=[(word,wght)]
            ind+= 1
        # check if terms in query
        if self.keep_originals:
            originals = []
            for (word,weight) in top_k:
                if word in q:
                    # original query terms weighted differently
                    originals.append((word,self.lmbda*(1/len(q))+weight))
            # first drop the originals
            top_k = [(word,weight) for (word,weight) in top_k if not word in q]
            # now add them again
            top_k.extend(originals)
            # and trim the top k
            top_k = sorted(top_k, key=lambda x :x[1], reverse=True)
            top_k = top_k[:k]
        else:
            top_k = [(word,weight) for (word,weight) in top_k if not word in q][:k]
        # weight them
        w_sum = 0
        for res in top_k:
            w_sum+=res[1]
        top_k = [(word, weight/w_sum) for (word,weight) in top_k]
        # build query_string
        return build_weighted_query(top_k)


