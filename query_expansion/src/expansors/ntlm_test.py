from expansors.ntlm import NTLMExpansor
from src.embeddings import word2vec

w2v = word2vec.Word2VecEmbeddings()
expansor = NTLMExpansor(embeddings=w2v, keep_originals=True, threshold=10)

# normal queries (strings)
query = "nuclear power"
# query = "nuclear abortion power"
# query = "nuclear power law"
# query = "nuclear power plants should be illegal"
# query = "should I abort my child"

print("expanding query...")
print(query)
print(expansor.expand(query))
