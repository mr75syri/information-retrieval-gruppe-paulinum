from expansors.abstract import Expansor
import fasttext
import nltk

class BaselineExpansor(Expansor):
    def __init__(self, embeddings, lmbda=0.1, keep_originals=True, threshold=3):
        super(BaselineExpansor, self).__init__(embeddings, keep_originals, threshold)

    def expand(self, query):
        """
        expand query
        :param query:
        :return:
        """
        expandedQuery =""
        for word in query.split(" "):
            if expandedQuery != "":
                expandedQuery += " OR " + self.extendWithSynonyms(word)
            else:
                expandedQuery += self.extendWithSynonyms(word)
        return expandedQuery


    def extendWithSynonyms(self, word):
        """
        transform word to synonym expanded word.
        e.g. dog -> (dog OR animal OR pet OR pitbull OR ...)
        :param word:
        :return: expanded word
        """
        synonymList = self.findSynonym(word)
        if len(synonymList) == 1:
            return word

        basicBoost = "^0.5"
        result = "(" + word + "^1"
        for synonym in synonymList:
            result += " OR " + synonym + basicBoost
        result += ")"
        return result

    def findSynonym(self, word):
        """
        Finding synonym for the given word
        :param word:
        :return: synonyms
        """
        synonymList = []
        neighbors = self.embeddings.model.get_nearest_neighbors(word, k=10)
        for neighbor in neighbors:
            if nltk.edit_distance(word,neighbor[1]) >= len(word):
                synonymList.append(neighbor[1])

        return synonymList