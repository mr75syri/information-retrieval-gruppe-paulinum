from abc import ABC, abstractmethod

from tools.tdmatrix import TDMatrix


class Expansor(ABC):
    """ an abstract base class for all expansion methods"""

    def __init__(self, embeddings=None, keep_originals=True, threshold=3):
        """

        :param embeddings: expansor typically needs embeddings
        :type embeddings: a class that implements embeddings.abstract.Embeddings
        :param keep_originals: should the returned queries also contain the original query terms (provided they are
                ranked high enough) ?
        :param tf_threshold: only return terms that occur in at least treshold documents
        """
        self.embeddings = embeddings
        self.keep_originals = keep_originals
        self.threshold = threshold
        # needed to check the threshold
        self.tdmatrix = TDMatrix()

    @abstractmethod
    def expand(self, query):
        """

        :param query: the query to expand
        :type query: string
        :return: the expanded query
        :rtype: string in Lucene-format
        """