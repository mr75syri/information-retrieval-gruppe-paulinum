from embeddings.word2vec import Word2VecEmbeddings
from expansors.abstract import Expansor
from tools.query import build_weighted_query
import time

class NTLMExpansor(Expansor):
    """
    implements the expansor proposed by Zuccon, Koopman, Burza and Azzopardi (paper 13 in the wiki)
    """

    def __init__(self, embeddings, keep_originals, threshold):
        super(NTLMExpansor, self).__init__(embeddings, keep_originals, threshold)
        self.embeddings = Word2VecEmbeddings()

    def expand(self, query):
        start_time = time.time()
        # split query in tokens
        q = query.split(" ")
        # eliminate tokens that are not in vocab
        q_clean = [word for word in q if not (self.embeddings.get_word_id(word) is None)]
        # nothing to expand?
        if len(q_clean) == 0:
            return ""

        # words and weights as tuples that will be used for the expanded query
        # original query words get the value 1.0 (see below!)
        expanded_terms = dict()

        candidates = list()

        for word in q_clean:
            # store these words first only so they appear in the result first
            # maybe not really relevant but felt better...
            expanded_terms[word] = 1.0

            # print(word, self.tdmatrix.idf(word))

            # pre query word: get the candidates that could be translated
            # here, drop words that occur only <= threshold times in the vocab
            top_k_clean = [(wrd, weight) for (wrd, weight) in self.embeddings.top_k(word, k=10) if
                           self.tdmatrix.contains_word(wrd) and self.tdmatrix.idf(wrd) > self.threshold]
            for tup in top_k_clean:
                # only use the words [0], not their values [1] from top_k
                candidates.append(tup[0])

        print("candidates:")
        print(candidates)
        print(" ")

        highest_idf = 0
        for new_word in candidates:
            highest_idf = max(highest_idf, self.tdmatrix.idf(new_word))

        for new_word in candidates:
            # value for the possible translation (new_word) of the orig query
            val = 0
            for word in q_clean:
                # (dict) get the (over all candidates!?) normalized cosine similarity per candidate
                cos_sim = self.get_normalized_p_cos(new_word, word, candidates)
                # multiply this similarity with p_u_d; here we use the terms idf, normalized with the local highest idf
                p_u_d = self.tdmatrix.idf(new_word) / highest_idf
                val += cos_sim * p_u_d
            # expanded_terms.append((new_word, val))
            if new_word in expanded_terms:
                if val > expanded_terms[new_word]:
                    expanded_terms[new_word] = val
            else:
                expanded_terms[new_word] = val

        # initial weights may have been overwritten by the above calculation; set to 1.0 again (yes, not the orig value)
        for word in q_clean:
            expanded_terms[word] = 1.0

        # print(" ")
        # print(expanded_terms)
        # print(" ")

        print(" ")
        print("time:" + str(time.time() - start_time))
        print(" ")

        return build_weighted_query(expanded_terms)

    def get_normalized_p_cos(self, w1, w2, candidates):
        """
        use a normalized cosine similarity to get the similarity of a word and a list of words (possible translations)
        :param w1: word (string) - should be in candidates?
        :param w2: word (string)
        :param candidates: list of candidates (strings)
        :return: normalized cosine similarity (float)
        """

        # make sure both words exist in the vocabulary
        if not (self.embeddings.is_in_vocab(w1) and self.embeddings.is_in_vocab(w2)):
            return 0

        sum = 0

        for candTuple in candidates:
            cand = candTuple[0]
            # similarity between words
            if self.embeddings.is_in_vocab(w2) and self.embeddings.is_in_vocab(cand):
                s = self.embeddings.sim(cand, w2)
                sum += s
            else:
                print("word is not in the vocabulary:" + w2 + " or " + cand)

        # all cos similarities are 0 ... should not happen
        if sum == 0:
            return 0

        # normalize over all possible translations
        return float(self.embeddings.sim(w1, w2) / sum)
