from src.embeddings import word2vec
from src.expansors.baseline import BaselineExpansor
# create Expansor
expansor = BaselineExpansor(word2vec.Word2VecEmbeddings())

print(expansor.expand("abortion"))

print(expansor.expand("communism"))

print(expansor.expand("donald trump good president"))

print(expansor.expand("Abortion should be legal for all the time?"))
