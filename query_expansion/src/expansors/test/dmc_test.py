#from embeddings.word2vec import Word2VecEmbeddings
from embeddings.dim600_16bit import Dim600Embeddings
from expansors.dmc import DMCExpansor

# create Expansor
expansor = DMCExpansor(Dim600Embeddings())

print(expansor.expand("nuclear power"))

print(expansor.expand("communism"))

print(expansor.expand("donald trump good president"))

# now without the original terms

expansor = DMCExpansor(Dim600Embeddings(), keep_originals=False)

# runs very long...
print(expansor.expand("nuclear power"))