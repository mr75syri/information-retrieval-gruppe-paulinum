from abc import ABC, abstractmethod

class Embeddings(ABC):
    """ an abstract base class for all embeddings-classes (we might try different types of embeddings) """

    def __init__(self, model_file):
        """
        :param model_file: path to the file containing the model
        """
        self.model_file = model_file


    @abstractmethod
    def top_k(self, word, k=50):
        """ top k similar words
        :param word: the word for which similar words should be retrieved
        :param k: how many similar words?
        :returns: top k similar words
        :rtype: ordered list of (word, sim) tuples
        """


    @abstractmethod
    def sim(self, w1, w2, method=None):
        """ similarity measure
        :param w1: word
        :param w2: word
        :param method: allows to specify a similarity measure
        :type method: string or None
        :returns: similarity between words w1 and w2
        :rtype: float
        """

    @abstractmethod
    def get_embedding(self, word):
        """ lookup a word embedding
        :param word: word to lookup the embedding for
        :returns: the embedding of word
        :rtype: np-array of floats, if word in vocab. Else None
        """

    @abstractmethod
    def get_name(self):
        """
        :returns: a unique name for the used model (maybe needed for evaluation...)
        """

    @abstractmethod
    def get_vocabulary_size(self):
        """
        not sure if needed...
        :return: size of the vocabulary
        :rtype: int
        """

    @abstractmethod
    def get_dim(self):
        """
        not sure if needed...
        :return: dimension of embeddings
        :rtype. int
        """

    @abstractmethod
    def is_in_vocab(self, word):
        """
        checks, wether there is a vector for the given word
        :param word: the word in question
        :return: True, if contained, False if not
        """

    @abstractmethod
    def get_matrix(self):
        """

        :return: the word-vec-matrix
        :rtype: numpy-nd-array
        """

    @abstractmethod
    def get_word_id(self, word):
        """

        :param word: the word in question
        :return: the line of the word in the word-vec-matrix
        """

    @abstractmethod
    def id_2_word(self,id):
        """
        maps an id to a word
        :param id: the id (int)
        :return: the word, if id exists, else None
        """