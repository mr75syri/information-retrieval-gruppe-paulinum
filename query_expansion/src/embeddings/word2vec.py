import fasttext
from scipy.spatial.distance import cosine
from sklearn.metrics.pairwise import  cosine_similarity
import os

from definitions import RES_DIR
from embeddings.abstract import Embeddings


class Word2VecEmbeddings(Embeddings):
    """ implements an interface to the word2vec embeddings"""


    def __init__(self): 
        super(Word2VecEmbeddings, self).__init__(RES_DIR+os.path.sep+"embeddings.bin")
        self.model = fasttext.load_model(self.model_file)



    def check_vocabulary(self, word):
        """
        raises a LookupError, if word is not in vocabulary
        only needed internally. Use is_in_vocab to "ask" the model if a word is contained in its vocabulary
        :param word: word to check
        :return: None
        """
        if not word in self.model:
            raise LookupError(str(word) + " not in vocabulary")


    def top_k(self, word, k=50):
        """ top k similar words
                :param word: the word for which similar words should be retrieved
                :param k: how many similar words?
                :returns: top k similar words
                :rtype: ordered list of (word, sim) tuples
        """
        self.check_vocabulary(word)
        return [(w,sim) for (sim,w) in self.model.get_nearest_neighbors(word, k=k)]


    def sim(self, w1, w2, method=None):
        """ similarity measure
        :param w1: word
        :param w2: word
        :param method: allows to specify a similarity measure
            here, only cosine implemented, so just leave as None
        :type method: string or None
        :returns: similarity between words w1 and w2
        :rtype: float
        """
        # words in vocab?
        self.check_vocabulary(w1)
        self.check_vocabulary(w2)
        # get their vectors
        vec1 = self.model[w1]
        vec2 = self.model[w2]
        # return the cosine similarity (1- cosdistance)
        return cosine_similarity(vec1.reshape(1,-1), vec2.reshape(1,-1))[0]




    def get_embedding(self, word):
        """ lookup a word embedding
        :param word: word to lookup the embedding for
        :returns: the embedding of word
        :rtype: np-array of floats, if word in vocab. Else None
        """
        if not self.is_in_vocab(word):
            return None
        return self.model[word]


    def get_name(self):
        """
        :returns: a unique name for the used model (maybe needed for evaluation...)
        """
        return "word2vec_skipgram"


    def get_matrix(self):
        """

        :return: the word-vec-matrix
        :rtype: numpy-nd-array
        """
        return self.model.get_output_matrix()


    def get_word_id(self, word):
        """

        :param word: the word in question
        :return: the line of the word in the word-vec-matrix. If not in vocab, None
        """
        if self.is_in_vocab(word):
            return self.model.get_word_id(word)
        return None 


    def get_vocabulary_size(self):
        """
        not sure if needed...
        :return: size of the vocabulary
        :rtype: int
        """
        return len(self.model.get_labels())


    def get_dim(self):
        """
        not sure if needed...
        :return: dimension of embeddings
        :rtype. int
        """
        return self.model.get_dimension()


    def is_in_vocab(self, word):
        """
            checks, wether there is a vector for the given word
            :param word: the word in question
            :return: True, if contained, False if not
        """
        return word in self.model

    def id_2_word(self,id):
        """
        maps an id to a word
        :param id: the id (int)
        :return: the word, if id exists, else None
        """
        if id>len(self.model.get_labels()):
            return None
        else:
            return self.model.get_labels()[id]


