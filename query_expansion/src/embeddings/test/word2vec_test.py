from embeddings.word2vec import Word2VecEmbeddings

# not really a test, more of a sanity check to see if everything works

embeddings = Word2VecEmbeddings()

# see if vocabulary check works
for w in ["the", "opponent", "qoprhbvlaboh"]:
    print("\""+w+"\""+ " contained in vocabulary? "+str(embeddings.is_in_vocab(w)))

print()
# get_dimension (should be 100)
print("Dimension: ", str(embeddings.get_dim()))

print()
# vocabulary size (should be around 80000)
print("Vocabulary Size: ", str(embeddings.get_vocabulary_size()))

print()
# get some top-k-similarities
top3the = embeddings.top_k("argument", k=10)
print("Top 10 similar words for \"argument\": ")
for w,sim in top3the:
    print(w,str(sim))

print()
# test the sim-method
ws1 = ["argument"]
ws2 = ["argument", "refute", "rebuttal", "address", "book", "unrelated", "test"]
for w1 in ws1:
    for w2 in ws2:
        print("SIM("+w1+","+w2+"): ", str(embeddings.sim(w1,w2)))


# are the id to word and word to id methods working?
for word in ["the", "argument", "test"]:
    assert(embeddings.id_2_word(embeddings.get_word_id(word))==word)