from sklearn.metrics.pairwise import  cosine_similarity
import os
import numpy as np
import pickle

from definitions import RES_DIR
from embeddings.abstract import Embeddings

class Dim600Embeddings(Embeddings):
    """ implements an interface to embeddings of dimension 600,
        composed of the word2vec-Embeddings (dim 300) and pretrained
        fasttext embeddings (dim 300). For details see report"""

    def __init__(self):
        super(Dim600Embeddings, self).__init__(RES_DIR+os.path.sep+"d600_matrix.npy")
        self.matrix = np.load(self.model_file)
        self.word2ind = pickle.load(open(RES_DIR+os.path.sep+"dim_600_word2ind.p","rb"))
        self.ind2word = pickle.load(open(RES_DIR+os.path.sep+"dim_600_ind2word.p", "rb"))
        print("model loaded")


    def check_vocabulary(self, word):
        """
        raises a LookupError, if word is not in vocabulary
        only needed internally. Use is_in_vocab to "ask" the model if a word is contained in its vocabulary
        :param word: word to check
        :return: None
        """
        try:
            x= self.word2ind[word]
        except:
            raise LookupError(str(word) + " not in vocabulary")


    def top_k(self, word, k=50):
        """ top k similar words
                :param word: the word for which similar words should be retrieved
                :param k: how many similar words?
                :returns: top k similar words
                :rtype: ordered list of (word, sim) tuples
        """
        self.check_vocabulary(word)
        # memory-friendly version, runs rather long...
        sims = np.zeros(shape=(self.get_vocabulary_size(),))
        wordvec = self.matrix[self.word2ind[word]].reshape(1, -1)
        # compare in batches
        batch_size = 5000
        num_batches = self.get_vocabulary_size()//batch_size
        if self.get_vocabulary_size()%batch_size!=0:
            num_batches+=1
        for i in range(num_batches):
            batch_start = i*batch_size
            batch_end = min(self.get_vocabulary_size(), (i+1)*batch_size)
            batch_sims = cosine_similarity(self.matrix[batch_start:batch_end], wordvec).flatten()
            sims[batch_start:batch_end] = batch_sims
            print(str(i), end="\r")
        tops = sims.argsort(axis=0)
        top_k = tops[-(k + 1):-1]
        res = []
        for index in top_k:
            w = self.ind2word[index]
            s = sims[index]
            res += [(w,s)]
        res.reverse()
        return res


    def sim(self, w1, w2, method=None):
        """ similarity measure
        :param w1: word
        :param w2: word
        :param method: allows to specify a similarity measure
            here, only cosine implemented, so just leave as None
        :type method: string or None
        :returns: similarity between words w1 and w2
        :rtype: float
        """
        # words in vocab?
        self.check_vocabulary(w1)
        self.check_vocabulary(w2)
        # get their vectors
        # return the cosine similarity
        e1 = self.matrix[self.word2ind[w1]]
        e2 = self.matrix[self.word2ind[w2]]
        return cosine_similarity(e1.reshape(1, -1), e2.reshape(1, -1))[0][0]


    def get_embedding(self, word):
        """ lookup a word embedding
        :param word: word to lookup the embedding for
        :returns: the embedding of word
        :rtype: np-array of floats, if word in vocab. Else None
        """
        if not self.is_in_vocab(word):
            return None
        return self.matrix[self.word2ind[word]]


    def get_name(self):
        """
        :returns: a unique name for the used model (maybe needed for evaluation...)
        """
        return "dim600_word2vec_16bit"


    def get_matrix(self):
        """
        cf. https://www.programcreek.com/python/example/99240/gensim.models.KeyedVectors.load_word2vec_format
        :return: the word-vec-matrix
        :rtype: numpy-nd-array
        """
        return self.matrix


    def get_word_id(self, word):
        """

        :param word: the word in question
        :return: the line of the word in the word-vec-matrix. If not in vocab, None
        """
        if self.is_in_vocab(word):
            return self.word2ind[word]
        return None


    def get_vocabulary_size(self):
        """
        not sure if needed...
        :return: size of the vocabulary
        :rtype: int
        """
        return len(self.word2ind.items())


    def get_dim(self):
        """
        not sure if needed...
        :return: dimension of embeddings
        :rtype. int
        """
        return self.matrix.shape[1]


    def is_in_vocab(self, word):
        """
            checks, wether there is a vector for the given word
            :param word: the word in question
            :return: True, if contained, False if not
        """
        try:
            self.check_vocabulary(word)
            return True
        except:
            return False


    def id_2_word(self,id):
        """
        maps an id to a word
        :param id: the id (int)
        :return: the word, if id exists, else None
        """
        try:
            word = self.ind2word[id]
            return word
        except:
            return None