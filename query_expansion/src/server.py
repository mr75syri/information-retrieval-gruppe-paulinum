from flask import Flask, request

from embeddings import word2vec
from expansors.baseline import BaselineExpansor
from expansors.dmc import DMCExpansor
from expansors.ntlm import NTLMExpansor

app = Flask(__name__)

w2v = word2vec.Word2VecEmbeddings()

baselineExpansor = BaselineExpansor(w2v)
dmcExpansor = DMCExpansor(embeddings=w2v, keep_originals=True)
ntlmExpansor = NTLMExpansor(embeddings=w2v, keep_originals=True, threshold=1)

@app.route('/api/query/expand/baseline', methods=['POST'])
def baselineExpansion():
    query = request.json["query"]
    return baselineExpansor.expand(query)

@app.route('/api/query/expand/dmc', methods=['POST'])
def dmcExpansion():
    query = request.json["query"]
    return dmcExpansor.expand(query)


@app.route('/api/query/expand/ntlm', methods=['POST'])
def ntlmExpansion():
    query = request.json["query"]
    return ntlmExpansor.expand(query)

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=False)
