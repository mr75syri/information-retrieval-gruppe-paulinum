# Information Retrieval - Argument Retrieval

## Project Architecture

* search-core-backend: Spring Boot Project which implements search interfaces on top of Apache Lucene
* query_expansion: Service for the query expansion
* clustering: Service for the clustering
* deployment: Basic deployment setup

### Run the Search Engine

The easiest way to run the whole application is to start all docker containers. To do this run the run.sh of the deployment folder. This script will downloading all needed resources. Afterwards all the container will be started.

### Run Queries

Example call:

```shell
curl -X POST 'http://localhost:8080/api/search?searchPhrase' 
        -d {	
        "similarity": "BM25",
        "query": "Speed limit are wrong",
        "sortByQuality": false,
        "expansionStrategy" : "NONE", 
        "resultSetSize":10
    }
```

For a detail describtion of the request parameter have a look into the report. 

## Start single components as seperate service

#### Requirements

For preparing the data, building and running the application you need:

- [Python](https://www.python.org/)
- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3](https://maven.apache.org)

#### Running the java application locally

First take a look into the `application.properties` file `search-core-backend/src/main/resources`. This file containing some configuration parameters.

- path_to_import_file: here you have to insert the file path to the corpus csv relative to the `search-core-backend` folder or moving the converted csv to the default folder (./import/origin_data.csv).
- indexLocation: under this path the created index will be stored locally on your machine 

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `org.leipzig.temir.air.AirApplication` class from your IDE.

Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like:

```shell
mvn spring-boot:run
```

#### Running the query expansion and clustering service

Both services containing a server.py. For starting the service you have to run the server.py.

Note: For the query expansion service you need the library fastText. The pip installation contains some issues. For the installation you have to following the instructions of [the project site](https://github.com/facebookresearch/fastText/)