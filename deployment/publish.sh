#!/bin/bash

## building and pushing search core backend service
cd ../search-core-backend && mvn clean -Dmaven.test.skip=true package
docker build -t matodd/ad-ir-search-core .
docker push matodd/ad-ir-search-core

## building and pushing query expansion service
cd ../query_expansion
docker build -t matodd/ad-ir-query-expansion .
docker push matodd/ad-ir-query-expansion

## building and pushing clustering service
cd ../clustering
docker build -t matodd/ad-ir-clustering .
docker push matodd/ad-ir-clustering