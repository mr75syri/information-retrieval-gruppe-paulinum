#!/bin/bash

##########################
# cleanup old containers #
##########################
echo  ">>> cleaning up the enviroment ..."
docker rm $(docker ps -a -q)

########################################################
# downloading the corpus and copy to /resources/corpus #
########################################################

#### demo file #####
#filePath=https://speicherwolke.uni-leipzig.de/index.php/s/cLCM5QfN2kmD97G/download

#### origin file #####
filePath=https://speicherwolke.uni-leipzig.de/index.php/s/bkqLzb7gjSFdGXD/download

echo ">>> downloading $filePath ..."
mkdir -p ./resources/corpus
wget $filePath -O ./resources/corpus/corpus.csv


#########################################
# downloading query expansion resources #
#########################################
mkdir -p ./resources/queryExpansion
wget https://speicherwolke.uni-leipzig.de/index.php/s/rWjA7AHEMPPRdby/download -O ./resources/queryExpansion/arg2id.p
wget https://speicherwolke.uni-leipzig.de/index.php/s/aBTTnTHfnwBHGGC/download -O ./resources/queryExpansion/embeddings.bin
wget https://speicherwolke.uni-leipzig.de/index.php/s/eMLz99CKFqsnGfM/download -O ./resources/queryExpansion/idf.p
wget https://speicherwolke.uni-leipzig.de/index.php/s/3TN3wiRqQ3RBfY4/download -O ./resources/queryExpansion/tdmatrix.npz
wget https://speicherwolke.uni-leipzig.de/index.php/s/LSX8HWSnj8K5rxq/download -O ./resources/queryExpansion/word2id.p
wget https://speicherwolke.uni-leipzig.de/index.php/s/eRKxg64Z6ypdnQw/download -O ./resources/queryExpansion/d600_matrix.npy

################################
# starting the container setup #
################################
docker-compose up