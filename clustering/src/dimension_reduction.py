from nltk.corpus import stopwords
from gensim.models import LdaModel
from gensim.models import LsiModel
from tdmatrix import TDMatrix
import numpy as np
from argsme import Argsme


class Reducer:
    """
    class for dimension reduction. Provides LDA and LSA
    """

    def __init__(self):
        self.tdmatrix = TDMatrix()
        # print(list(stopwords.words('english')))
        known_stopwords = [sw for sw in list(stopwords.words('english')) if sw in self.tdmatrix.word2id.keys()]
        self.common_stopwords = [self.tdmatrix.word2id[sw] for sw in known_stopwords]
        # dict to look up argument texts
        self.args = Argsme()


    def _arg2bow(self, arg_id):
        """
        converts the argument specified  by arg_id in bow-representation (needed for LSA and LDA) 
        
        :param arg_id: an argsme ID 
        :return: the bow representation of the given argument 
        :rtype: list of (word_id, frequency) tuples 
        """
        tf_row = None
        try:
            tf_row = self.tdmatrix.get_doc_row(arg_id)
        except:
            raise AttributeError("no valid argsme-ID")
        # row is not in a suitable format yet...
        tf_row = tf_row.tolist()[0]
        # now create the BOW-Representation
        doc = []
        for i in range(len(tf_row)):
            if (tf_row[i] != 0):
                doc += [(i, tf_row[i])]
        return doc


    def _corpus2bow(self, args_list, filter_common_stopwords=True, filter_corpus_stopwords=False,
                    corpus_stopword_thresh = 0.5):
        """
        converts a set of argsme-Arguments to bow representation, optionally filtering out common and corpus-specific
        stopwords

        :param args_list: list of argsme-IDS
        :type args_list: list of strings
        :param filter_common_stopwords: set True to filter out the most common english stopwords
        :param filter_corpus_stopwords: set True to filter out stopwords with respect to the corpus
        :param corpus_stopword_thresh: only relevant, if filter_corpus_stopwords. All words that occur in
            > corpus_stopword_thresh % of the documents are filtered out
        :returns corpus in BOW format
        :rtype list of list of (word_id, freq)-tuples

        """

        # convert all single arguments to bow-representation
        corpus_bow = [self._arg2bow(arg_id) for arg_id in args_list]
        # filter out common stopwords
        if filter_common_stopwords:
            corpus_bow = [[w for w in doc if not (w[0] in self.common_stopwords)] for doc in corpus_bow]
        # filter corpus stopwords
        if filter_corpus_stopwords:
            idf_dict = {}
            for doc in corpus_bow:
                words = set([w[0] for w in doc])
                for word in words:
                    if word in idf_dict.keys():
                        idf_dict[word]+=1
                    else:
                        idf_dict[word]=1
            threshold = int(len(corpus_bow)*corpus_stopword_thresh)
            # filter them out
            corpus_bow = [[w for w in doc if idf_dict[w[0]]<=threshold] for doc in corpus_bow]
        return corpus_bow


    def perform_reduction(self, args_list, method="lsa", k=4, filter_common_stopwords=True, filter_corpus_stopwords=False,
                    corpus_stopword_thresh = 0.5):
        """
        reducts dimension of the term document matrix of a set of argsme-Arguments using Latent Dirichlet

        :param args_list: list of argsme-IDS
        :type args_list: list of strings
        :param method: the method used to reduce the term document matrix, either "lda" or "lsa"
        :param k number of dimensions/topics
        :param filter_common_stopwords: set True to filter out the most common english stopwords
        :param filter_corpus_stopwords: set True to filter out stopwords with respect to the corpus
        :param corpus_stopword_thresh: only relevant, if filter_corpus_stopwords. All words that occur in
                    > corpus_stopword_thresh % of the documents are filtered out
        :returns reduced term document matrix
        :rtype list of vector representations

        """
        corpus = self._corpus2bow(args_list, filter_common_stopwords, filter_corpus_stopwords, corpus_stopword_thresh)
        model = None
        if method=="lda":
            model = LdaModel(corpus, num_topics=k)
        elif method=="lsa":
            # takes lot of memory, be careful
            model = LsiModel(corpus, num_topics=k, power_iters=1000)
        else:
            raise AttributeError("method must be either 'lsa' or 'lda'")
        # return the representations
        representations = []
        for i in range(len(corpus)):
            doc_row = model[corpus[i]]
            # print(doc_row)
            representations += [doc_row]
        return self._extend_representation(representations, k)


    def _extend_representation(self, rep, k):
        """
        Gensim models only return a compressed vector as a list of form [(i, vec)]
        This method converts such lists into a numpy-matrix
        :param rep: the representation yielded by a gensim model
        :param k: the length of the vector to be returned
        :returns the representation as vectors
        :rtype: numpy.ndarray of dimension (|doc|,k)
        """

        result = [[0. for _ in range(k)] for _ in range(len(rep))]
        for r in range(len(rep)):
            for (ind, val) in rep[r]:
                result[r][ind] = val
        return np.array(result)
