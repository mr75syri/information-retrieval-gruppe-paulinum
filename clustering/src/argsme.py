import json
from definitions import RES_DIR
import os

class Argsme:
    """
    interface to the argsme corpus
    needed to return the texts for argsme ids
    """

    def __init__(self):
        corpus = json.load(open(RES_DIR+os.sep+"args-me.json"))
        self.arg_dict = {}
        for i in range(len(corpus['arguments'])):
            arg_id = corpus['arguments'][i]['id']
            arg_text = corpus['arguments'][i]['premises'][0]['text']
            self.arg_dict[arg_id] = arg_text

    def get_text(self, argsme_id):
        """
        returns the text of an argument, given the id

        :param argsme_id: the id of the argument as string
        :return: the argument text
        :rtype string
        """
        try:
            return self.arg_dict[argsme_id.strip()]
        except:
            return None