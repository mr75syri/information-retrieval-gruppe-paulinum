from flask import Flask, request
from optimizers.brute_force import Brute_Force_Optimizer
from flask import jsonify
from argsme import Argsme


app = Flask(__name__)
args_dict = Argsme()

@app.route('/api/query/clustering/lda', methods=['POST'])
def lda_clustering():
    return rerank(request, method="lda")

@app.route('/api/query/clustering/lsa', methods=['POST'])
def lsa_clustering():
    return rerank(request, method="lsa")


def rerank(request, method="lda"):
    """
    executes the reranking with default alpha=0.5 and max_seconds=100
    (can be changed in the request)

    :param request  the flask request
    :param method  method used for pseudo-clustering, either "lsa" or "lda"
    """
    args_list = request.json["ids"]
    ratings = request.json["ratings"]
    alpha = 0.5
    if request.json["alpha"]:
        alpha = request.json["alpha"]
    max_seconds= 100
    if request.json["max_seconds"]:
        max_seconds = int(request.json["max_seconds"])
    permutation = Brute_Force_Optimizer(args_list, ratings, alpha).optimize(max_seconds)
    return unpack_perm(permutation, args_list)


def unpack_perm(permutation, args_list):
    """
    converts the returned data from an optimizer to a JSON response

    :param permutation: the permutation as list
    :param args_list: list of argument ids

    :returns JSON response of type [{"id":...,"argument":},...]
    :rtype flask Response object
    """
    ls = []
    for ind in permutation:
        arg_id = args_list[permutation[ind]]
        arg_text = args_dict.get_text(arg_id)
        ls+= [{"id":arg_id, "argument":arg_text}]
    return jsonify(ls)

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=False)
