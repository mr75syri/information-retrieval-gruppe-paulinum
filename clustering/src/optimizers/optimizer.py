from abc import ABC, abstractmethod
from dimension_reduction import Reducer
from scipy.spatial.distance import cosine
import numpy as np

class Optimizer(ABC):
    """
    an abstract base class for Optimizers
    """

    def __init__(self, args_list, ratings, alpha=0.5):
        vectors= Reducer().perform_reduction(args_list)
        self.args_list = args_list
        # the parameter alpha
        self.alpha = alpha
        # the distance matrix
        self.dist = [[cosine(vectors[i], vectors[j]) for j in range(len(vectors))] for i in range(len(vectors))]
        # normalize the ratings
        self.ratings = self._min_max_normalize(ratings)
        self.num_docs = len(self.ratings)
        # precompute the logs
        self.logs = [np.log2(i) for i in range(2, len(args_list) + 2)]


    def _min_max_normalize(self,values):
        mn = min(values)
        mx = max(values)
        return [(v-mn)/(mx-mn) for v in values]


    @abstractmethod
    def optimize(self, max_seconds=100):
        """
        the actual optimizer

        :param max_seconds: for optimizer that may take longer (restricts time, may be ignored)
        :return the optimized ranking
        :rtype ordered list (descending) of args-ids (as strings) if solution found, None else
        """

