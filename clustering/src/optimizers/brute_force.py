from optimizers.optimizer import Optimizer
from itertools import permutations
from time import time
from random import shuffle

class Brute_Force_Optimizer(Optimizer):
    """
    solves the problem by trying out all combinations (reasonable for ~8 documents)
    """

    def __init__(self, args_list, ratings, alpha=0.5):
        super(Brute_Force_Optimizer, self).__init__(args_list, ratings, alpha=alpha)


    def optimize(self, max_seconds=100):
        """
        the actual optimizer

        :param max_seconds: for optimizer that may take longer (restricts time, may be ignored)
        :return the optimized ranking
        :rtype ordered list (descending) of args-ids (as strings) if solution found, None else
        """
        t0 = time()
        best_perm = tuple(range(self.num_docs))
        best_val = self._calculate_value(best_perm)
        exceeded = False
        counter = 0
        for permutation in permutations(range(self.num_docs)):
            # time bound exceeded
            if time() - t0 >= max_seconds:
                exceeded = True
                break
            else:
                val = self._calculate_value(permutation)
                if val >= best_val:
                    best_val = val
                    best_perm = permutation
                counter+=1
        if exceeded:
            print('Exceeded time limit of {} seconds, tried {} possible solutions'.format(max_seconds, counter))
        return best_perm


    def _calculate_value(self, ranking):
        """
        determines the value of the formula to be optimized for a given ranking

        :param ranking: the ranking to be evaluated as tuple or list of unique integers in [0, num_docs]
        :returns the value
        :rtype float
        """
        value = 0
        # add the quality rankings
        for i in range(len(ranking)):
            doc_id = ranking[i]
            value += (self.ratings[doc_id]/self.logs[i])
        value *= self.alpha
        # add the heterogenity rankings
        for j in range(1, len(ranking)):
            doc_id = ranking[j]
            for k in range(1, j+1):
                doc2 = ranking[j-k]
                value += (1-self.alpha)*(1/(j+1)) * (1/(k+1)) * self.dist[doc_id][doc2]

        return value



