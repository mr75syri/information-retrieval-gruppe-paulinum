
from optimizers.brute_force import Brute_Force_Optimizer
import pandas as pd
from definitions import RES_DIR
import os

data = pd.read_csv(RES_DIR+os.sep+"clusteringtest_ids.txt", header=None)
print(data)
data.columns=["ID"]
ids = data.ID.tolist()

o = Brute_Force_Optimizer(args_list=ids, ratings=[1-i*(1/len(ids)) for i in range(len(ids))], alpha=.0)
print(o.optimize(max_seconds=200))