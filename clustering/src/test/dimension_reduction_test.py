import pandas as pd
from definitions import RES_DIR
import os
from dimension_reduction import Reducer

# read the test corpus
data = pd.read_csv(RES_DIR+os.sep+"example_ids.txt", header=None)
print(data)
data.columns=["ID"]
ids = data.ID.tolist()
print(ids)

# Reducer
reducer = Reducer()

print(reducer.perform_reduction(ids))

print("with corpus threshold")
print(reducer.perform_reduction(ids, filter_corpus_stopwords=True))
print("with lower threshold")
print(reducer.perform_reduction(ids, filter_corpus_stopwords=True, corpus_stopword_thresh=0.25))

# high memory need, may not work on some platforms
print("with lsi")
print(reducer.perform_reduction(ids, method="lsa"))