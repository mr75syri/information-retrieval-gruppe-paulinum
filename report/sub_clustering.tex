
\subsection{Clustering and Reranking} 
A more experimental component of our search engine is the clustering/reranking module. 
\subsubsection{Motivation} 

In retrieving arguments, not only the argumentative quality of the returned results is important. Another aspect of an argument search engine's utility is the heterogeneity of the returned arguments. In every use case of such a search engine, the user benefits from receiving a wide variety of semantically different arguments.

A problem of the argsme corpus is that an argumentative document usually contains more than one argument. Nevertheless, documents may often be semantically similar. This can be demonstrated by a simple experiment. We take the first 8 results yielded by DPH for the evaluation query ``speed limits are wrong". For these results, the term document matrix is built. Hereby, we consider only nouns that are not contained in the query. We then compute the similarity matrix for the 8 documents by computing cosine similarities (based on the term document vectors) for every pair of documents. Table \ref{exp} shows the first two rows of the similarity matrix. The remaining six rows are omitted for the sake of brevity. 
\begin{table} 
\centering
\caption{Similarity matrix (first two rows) of 8 top documents $d_1,...,d_8$ returned by DPH for the query ``speed limit". }\label{exp}
\begin{tabular}{|c|c|c|c|c|c|c|c|c|}
\hline
 &  $d_1$ & $d_2$ & $d_3$ & $d_4$ & $d_5$ & $d_6$ & $d_7$ & $d_8$\\
\hline
$d_1$ & 1. & 0.19 & 0.22 & 0.12 & 0.16 & 0.09 & 0.15 & 0.19\\ 
 $d_2$ &  0.19 & 1. & 0.26 & 0.38 & 0.2 & 0.19 & 0.15 & 0.08\\
\hline
\end{tabular}
\end{table} 

Clearly, there are pairs of documents that are more similar to each other semantically (e.g. $d2$,$d_4$), while other pairs are quite dissimilar (e.g. $d_1$, $d_6$). 

Another problem is that optimizing heterogeneity can conflict with optimizing quality. Both goals need to be balanced.

%TODO hier Experiment zur Begründung...


\subsubsection{Related Work} 
 
 

Carbonell and Goldstein \cite{carbonell1998use} introduce Maximal Marginal Relevance (MMR), a measure that allows building a ranking incrementally. In every step, a document $d_i$ from a candidate set $R$ is added to the existing ranking $S$. This $d_i$ is of maximal marginal relevance: 
\begin{equation}
    MMR = argmax_{d_i\in R\backslash S}[\lambda(Sim_1(d_i,Q)) - (1-\lambda)max_{d_j\in S} Sim_2(d_i,d_j)]
\end{equation}
$(Sim_1(d_i,Q))$ can be seen as the quality of the document given the query $Q$, while $max_{d_j\in S} Sim_2(d_i,d_j)$ accounts for the ``novelty" of $d_i$ given the existing ranking $S$. 

Another approach to build a diverse ranking incrementally is proposed by Kaptein et al. \cite{kaptein2009result}. Here, a document is appended to the ranking, if it contains the most previously unseen terms.  

Deselaers et al. \cite{deselaers2009jointly} describe a method to diversify image search results. They define the novelty of an image $x^*$ given an existing ranking $(x_{r1},...,x_{rj})$ as 
\begin{equation}
    N(x^*,(x_{r_1},...,x_{r_j}))= \frac{1}{j}\Sigma_{i=1}^j d(x*,x_{r_i})
\end{equation} 
where $d$ is a distance measure.

Zhu et al. \cite{zhu2007improving} propose GRASSHOPPER, a ranking method focusing on diversity. GRASSHOPPER is based on random walks.

A problem in diversifying search results is that classic evaluation measures like (n)DCG do not take diversity of results into account. Clarke et al. \cite{clarke2008novelty} thus propose an alternative evaluation framework.
\subsubsection{Methods} 

As the CLEF task is evaluated using nDCG, we first make sure that our results are of high quality (w.r.t to the query and the argumentation quality). Then, the top 8 of them are clustered and reranked in order to diversify the top results. 

The semantic clustering is implemented using Latent Semantic Analysis (Deerwester et al. \cite{deerwester1990indexing}) for 3 topics. This provides a vector of size 3 for each of the top 8 documents. Now, $dist(d_1,d_2) = 1-SIM(d_1,d_2)$ describes the dissimilarity between two documents $d_1$ and $d_2$. For $SIM$ we choose the cosine similarity between the document vectors generated by LSA. 

In the following, let $R$ be the ranking and $R[i]$ the document with rank $i$ in $R$.

Similar to Deselaers et al. \cite{deselaers2009jointly}, we employ a notion of a document's novelty. Novelty of a document $R[i]$ is related to $R[i]'s$ predecessors in the ranking $R[1]...R[i-1]$: 
\begin{equation}
Nov(R[i]):= \Sigma_{k=1}^{i-1} \frac{1}{k}dist(R[i], R[i-k])
\end{equation}
We weight the dissimilarity depending on the number of ranks between $R[j]$ and $R[j-k]$: documents should not be similar to their immediate predecessor.  

Now we define a measure for $R$'s diversity/heterogeneity: 
\begin{equation}
    heterogeneity(R):=\Sigma_{j=2}^{|R|} \frac{1}{j}Nov(R[j])
\end{equation}
The likelihood that a user actually looks at a document $d$ decreases with $d$'s rank. Because of that, our heterogeneity measure weights each document's novelty depending on its rank. 

Next, heterogenity of a ranking $R$ needs to be balanced with $R$'s quality. To achieve this, we compute a reranking $R'$ of $R$ that maximizes 

\begin{equation} 
\label{cls}
    \gamma*quality(R') + (1-\gamma)*heterogeneity(R')
\end{equation} 

We use nDCG with our retrieval model's ratings to compute $quality(R')$. Note that the problem of finding an optimal $R'$ can be framed as Mixed Integer Program. However, since we restrict ourselves to reranking only the top 8 documents, we find an optimal solution using brute force. 
Table \ref{cluster_example} shows the effect of our reranking on a dummy corpus. 

\begin{table} 
\centering
\caption{Examples for reranking on a dummy subset of the argsme corpus, for different values of $\gamma$ in Equation \ref{cls}. In Brackets the dummy quality value for each ``argument".}\label{cluster_example}
\begin{tabular}{|l|l|l|}
\hline
Initial ranking ($\gamma=1.$) &  $\gamma=0.5$ & $\gamma=0$\\
\hline
Vote Con! (1.0) \cellcolor{yellow}& Vote Con! (1.0) \cellcolor{yellow}  &  Vote PRO. (0.625) \cellcolor{blue!20} \\
Vote Con (0.875) \cellcolor{yellow}&  Vote for Con. (0.75) \cellcolor{yellow} & Please extend ... (0.25)\cellcolor{orange!50} \\
Vote for Con. (0.75) \cellcolor{yellow}& Extend my arg... (0.375) \cellcolor{orange!50}& vote for pro (0.5) \cellcolor{blue!20}\\
Vote PRO. (0.625) \cellcolor{blue!20}& Vote Con (0.875) \cellcolor{yellow} & extend all arg... (0.125)\cellcolor{orange!50} \\
vote for pro (0.5) \cellcolor{blue!20}&  Please extend ... (0.25)\cellcolor{orange!50}&  Vote Con (0.875) \cellcolor{yellow}\\  
Extend my arguments. (0.375) \cellcolor{orange!50}& vote for pro (0.5) \cellcolor{blue!20} & Vote for Con. (0.75) \cellcolor{yellow}\\ 
Please extend all arguments (0.25)\cellcolor{orange!50} & Vote PRO. (0.625) \cellcolor{blue!20} & Extend my arg... (0.375) \cellcolor{orange!50}\\ 
extend all arguments (0.125)\cellcolor{orange!50} & extend all arg... (0.125)\cellcolor{orange!50} & Vote Con! (1.0) \cellcolor{yellow}\\ 
\hline
\end{tabular}
\end{table}
The hyperparameter $\gamma$ in Equation \ref{cls} could be set by the user. Alternatively, $\gamma$ could be further investigated in order find a reasonable value. This is beyond the scope of our project. For the evaluation, we turn off the clustering component (i.e. set $\gamma$ to 1), because nDCG does not consider heterogeneity. 








