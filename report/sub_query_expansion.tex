\subsection{Query Expansion}

A query is a short representation of the user's information need.
However, these few words may not be sufficient to encompass the entire concept that the user wants to express.
This can lead to highly relevant documents not being found by the retrieval system due to vocabulary missmatch.
That is, the user may choose terms for his query that do not appear in a relevant document.
%Instead, we focus on a few automatic query expansion (AQE) methods that can be used to mitigate the vocabulary missmatch gap.
To mitigate this gap, automatic query expansion (AQE) methods can be used.
In this section we briefly describe the components of AQE and our implementation.

\subsubsection{Related Work}
% text should not be on a new line
Several different approaches have been explored that aim to improve the user query so that more matching documents can be retrieved \cite{carpineto2012survey}.
%Some examples are pseudo-relevance feedback (assuming the top ranked documents of an initial retrieval step are relevant and contain terms that can be extracted for a subsequent retrieval iteration) or interactive query refinement that requires the user to readjust his query.
Some examples are pseudo-relevance feedback (using terms from the top ranked documents) or interactive query refinement that requires the user to readjust his query.
Another option is to use search query logs to obtain rewritings that users perform to improve their query terms with respect to their information need.
However, some of these methods are out of the scope for this work, or require additional data.
For instance, search query logs are unavailable for this task.
Therefore, we focus on a few AQE methods that are based on word embeddings.
This includes the work presented by Diaz et al. \cite{diaz2016query} as well as Zuccon et al. \cite{zuccon2015integrating} which we cover in the methods section.

%
Query Expansion comprises four steps.
First, the source data must be transformed so that it can be used effectively in the following steps.
This involves natural language preprocessing techniques such as tokenization and word stemming.
Second, expansion feature candidates have to be generated based on the input (the user query). % plus the source data...
There are different types of feature generation.
They can be produced word by word, e.g. finding synonyms, or from the entire query as a whole.
Some techniques use language modeling which we will refer to as ``model-based'' approaches.
In the next step, the obtained candidates have to be ranked and the best features will be selected for query expansion.
Last, the actual reformulation of the query takes place.
Usually, this involves weighting the terms that will be passed to the retrieval system.

\subsubsection{Methods}
% decided - past tense?
For our query expansion component we decided to use one simple baseline approach and two more sophisticated concepts.
First, we use a straightforward synonym generation feature, that fetches similar words for each individual query term.
This method can not grasp the concept of the entire query as one unit.
However, as multiple of the provided queries from the underlying task only consist of very few terms, such as ``speed limit'' or ``nuclear weapons'', this simple AQE method can potentially provide a useful enhancement.

% first expansion method
The other two expansion procedures both rely on word embeddings.
In our work we use fastText\footnote{https://fasttext.cc/} to obtain vector representations from the argsme corpus (dimension: 300).
In addition, we create another set of word embeddings by combining these locally trained representations with pre-trained models which fastText offers (combined dimension: 600).
Then, we adapt a query expansion method as proposed by Diaz et al. \cite{diaz2016query}.
This model-based expansion procedure searches the word embeddings for semantically similar terms in order to estimate an alternative to the original query by interpolating the query language model $p_q$ with that of the expansion language $p_{q^+}$ as follows:
%$p^1_q (w) = \lambda p_q (w) + (1-\lambda) p_{q^+} (w)$.
\begin{equation}
p^1_q(w) = \lambda p_q (w) + (1-\lambda) p_{q^+}(w)
\end{equation} 
All newly found terms are then weighted and the best ones (matching the modeled language) are selected to augment the query.

%
Even though the work presented by Zuccon et al. \cite{zuccon2015integrating} does not directly focus on AQE, we also use their insights to realize another expansion method.
They investigate different ways to estimate translation probabilities for terms that belong to the same language model.
In a similar manner, our goal for AQE becomes to find words $u$ that are likely to be translated from the initial query terms $w$:
%The translation probability is defined as
\begin{equation}
p_t(w|q) = \Sigma_{u\in q} p_t(w|u)p(u|q)
\end{equation} 
where $p_t(w|u)$ describes the probability of translating term $u$ into $w$ which can be approximated by a normalized cosine similarity.

Naturally, both expansion techniques operate on each query as a whole to incorporate their relatedness.
Eventually, the expansion terms and their respective weights are returned to our search core.
Note that for the current implementation only one expansion method is used at a time.
We refer to the latter approach as ``NTLM'' (to stay consistent with the source paper) and label the first one ``DMC''.