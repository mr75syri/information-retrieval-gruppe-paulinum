\section{Evaluation and Results}
\subsection{Experimental Setup}
To answer the questions which component and combination of components from our possible retrieval models effect the search result the most and provides the best results, we implement almost each pipeline step as a separate service.
For the experiments, we index our extended argument corpus with the quality ratings.
To identify which model performs best, we perform a manual evaluation.
For this manual study, we randomly choose three queries.
Afterwards, we extract 98 arguments of the corpus which are mapped to one of the three queries.
For the extraction process, the retrieval model was used.
In the following rating phase the information about which model retrieved which argument was hidden.
To rate each argument we use the metrics which are also used by Potthast et al. \cite{stein:2019j}, to evaluate not only the relevance but also the rhetoric, logic, and dialectic quality on a Likert scale between 1 and 4.
The relevance is rated with a Likert scale between 0 and 3.

We recognize that this study is not sufficient to answer the question which model is the best, as three queries are not enough.
But we hope that this experiment gives us a first impression on their performance.

Following, a combined relevance was used to generate the ranking.
The ideal ranking is the descending order of the combined ratings.
The combined relevance is needed because we initially also retrieve documents that are not related to the topic.
Independent from the dimensions rhetoric, logic and dialectic these documents should have a combined score of 0.
This could be only guaranteed with the following expression:
$relevance = r \cdot (rh + l +d)$
, where $r$ is the relevance, $rh$ the rhetoric, $l$ the logic, and $d$ the dialectic value.
The underlying intention is, that documents which don't match should have a relevance rating of 0.

Additionally, we create three more ideal rankings including the other dimensions.
This experiment setup gives us the possibility to evaluate each retrieval model very fast.
For each model, we evaluate the search results of the three queries by using DCG@5 and the nDCG@5 \cite{J_rvelin_2002}.

\subsection{Discussion}
\begin{figure}[h!]
\includegraphics[width=\textwidth]{result_table.pdf}
\caption{Results of the a) relevance and b) combined relevance evaluation.} \label{evaluation_results}
\end{figure}

Figure \ref{evaluation_results} shows the results of the first evaluation phase.
For this part, we consider a separate evaluation of the general retrieval model with our own extended combinations.
First of all, we want to consider only relevance scores.
BM25 model achieves the best results with the use of the quality ratings and the baseline query expansion strategy.
The baseline query expansion also reached the best results for the DPH model.
But it seems to better in this case (with these three specific queries) not to use the quality ratings.

The second part of the figure shows the result evaluation with regard to the combined relevance.
Also in this study, the BM25 model with a combination of the baseline query expansion and with use of the quality ratings yields the best values.
For the DPH model, the combination of the use of quality ratings and the DMC model provides the best results regarding the nDCG@5 score. 

This first part of the evaluation shows that it could make sense to include a quality rating in the retrieval model.
In this experiment, the $\alpha$ and $\beta$ parameter of the scoring function, as described in section \ref{sec_retrieval}, were both set to 0.5.
It appears that this combination is a good and balanced choice for the weighting of each part.
The results show that including the quality ratings results in ranking well written arguments higher than poorly formulated arguments.

The difference between the query strategies baseline, DMC and NTLM are visible.
The baseline and DMC strategy seem to perform the best in both cases with a statistical but not significant divergence.
Using the NTLM strategy, the results could be better.
In all cases, this strategy returned the worst arguments for a specific query.

The first part of this evaluation only includes judged arguments for three queries.
Furthermore, we conduct a second evaluation through tira.io in the context of the shared task. 
We choose the two basic retrieval models (BM25 and DPH) with the baseline query expansion because these combinations promise the best results.
Also, we include the combination of DPH and DMC to answer the question of how well the DMC query expansion performs.
We choose the DPH model based on the promising results of the first part of the evaluation.
All three models include the quality ratings for the score calculation.
A separate evaluation of a retrieval model which includes the NTLM query expansion is omitted for now.
It seems that this query expansion does not perform really well in the context of argument search.

\begin{table}[]
\centering
\caption{Results of the evaluation through tira.io}
\begin{tabular}{|l|l|l|l|l|}
\hline
retrieval model & nDCG & nDCG@5 & nDCG@10 & QRelCoverage@10 \\ \hline
DPH - Baseline & 0.007 & 0.0 & 0.018 & 0.1 \\ \hline
BM25 - Baseline & 0.033 & 0.052 & 0.087 & 0.4 \\ \hline
DPH - DMC & 0.006 & 0.008 & 0.016 & 0.08 \\ \hline
\end{tabular}
\label{tira-results}
\end{table}

The results of the second part of the evaluation are shown in Table \ref{tira-results}. 
Considering these values the combination of BM25 with the baseline query expansion is the best model.
This is in contradiction to the work presented by Potthast et al. \cite{stein:2019j}.
However, all measurements are relatively low.
We are uncertain about how meaningful these values are or why these measurements are so low.
Submitting random search queries against our search engine delivers useful and apparently decent arguments.
Including the quality ratings, the results contain more linguistically superior arguments.
However, we also repeatedly observe arguments in the result sets which are unrelated to the topic of the query when using different combinations and queries.
Perhaps this behavior occurs with more topics than expected.  
The other reason could be that our search engine does not include the user bias of a query.
That means that retrieved pro and contra arguments have a false stance with respect to the query.
%An evaluation in the use of the clustering-based reranking, unfortunately, we didn't trough tira.io.
We did not evaluate our clustering based reranking using tira.io.
However, independent tests show that this method has the potential to increase the quality of the result set.
It is possible, that a reranking based on the arguments increases the nDCG ratings of tira.io because this evaluation method includes the specific ranking of the result set.

An argument search engine that aims to deliver well written arguments, should include such quality ratings in the retrieval process.
