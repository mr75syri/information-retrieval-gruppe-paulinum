\begin{thebibliography}{10}
\providecommand{\url}[1]{\texttt{#1}}
\providecommand{\urlprefix}{URL }
\providecommand{\doi}[1]{https://doi.org/#1}

\bibitem{ajjour2019data}
Ajjour, Y., Wachsmuth, H., Kiesel, J., Potthast, M., Hagen, M., Stein, B.: Data
  acquisition for argument search: The args. me corpus. In: Joint
  German/Austrian Conference on Artificial Intelligence (K{\"u}nstliche
  Intelligenz). pp. 48--59. Springer (2019)

\bibitem{brysbaert2014concreteness}
Brysbaert, M., Warriner, A.B., Kuperman, V.: Concreteness ratings for 40
  thousand generally known english word lemmas. Behavior research methods
  \textbf{46}(3),  904--911 (2014)

\bibitem{carbonell1998use}
Carbonell, J., Goldstein, J.: The use of mmr, diversity-based reranking for
  reordering documents and producing summaries. In: Proceedings of the 21st
  annual international ACM SIGIR conference on Research and development in
  information retrieval. pp. 335--336 (1998)

\bibitem{carpineto2012survey}
Carpineto, C., Romano, G.: A survey of automatic query expansion in information
  retrieval. Acm Computing Surveys (CSUR)  \textbf{44}(1),  1--50 (2012)

\bibitem{clarke2008novelty}
Clarke, C.L., Kolla, M., Cormack, G.V., Vechtomova, O., Ashkan, A.,
  B{\"u}ttcher, S., MacKinnon, I.: Novelty and diversity in information
  retrieval evaluation. In: Proceedings of the 31st annual international ACM
  SIGIR conference on Research and development in information retrieval. pp.
  659--666 (2008)

\bibitem{deerwester1990indexing}
Deerwester, S., Dumais, S.T., Furnas, G.W., Landauer, T.K., Harshman, R.:
  Indexing by latent semantic analysis. Journal of the American society for
  information science  \textbf{41}(6),  391--407 (1990)

\bibitem{deselaers2009jointly}
Deselaers, T., Gass, T., Dreuw, P., Ney, H.: Jointly optimising relevance and
  diversity in image retrieval. In: Proceedings of the ACM international
  conference on image and video retrieval. pp.~1--8 (2009)

\bibitem{diaz2016query}
Diaz, F., Mitra, B., Craswell, N.: Query expansion with locally-trained word
  embeddings. arXiv preprint arXiv:1605.07891  (2016)

\bibitem{hutto2014vader}
Hutto, C.J., Gilbert, E.: Vader: A parsimonious rule-based model for sentiment
  analysis of social media text. In: Eighth international AAAI conference on
  weblogs and social media (2014)

\bibitem{10.1145/582415.582418}
J\"{a}rvelin, K., Kek\"{a}l\"{a}inen, J.: Cumulated gain-based evaluation of ir
  techniques. ACM Trans. Inf. Syst.  \textbf{20}(4),  422–446 (Oct 2002).
  \doi{10.1145/582415.582418}, \url{https://doi.org/10.1145/582415.582418}

\bibitem{kaptein2009result}
Kaptein, R., Koolen, M., Kamps, J.: Result diversity and entity ranking
  experiments: Anchors, links, text and wikipedia. Tech. rep., AMSTERDAM UNIV
  (NETHERLANDS) INTELLIGENT SYSTEMS LAB AMSTERDAM (2009)

\bibitem{moraes2017ictir-a}
Moraes, F., Santos, R.L.T., Ziviani, N.: On effective dynamic search in
  specialized domains. In: Proceedings of the 3rd ACM International Conference
  on the Theory of Information Retrieval. ACM, Amsterdam, The Netherlands
  (2017)

\bibitem{persing2017can}
Persing, I., Ng, V.: Why can't you convince me? modeling weaknesses in
  unpersuasive arguments. In: IJCAI. pp. 4082--4088 (2017)

\bibitem{stein:2019j}
Potthast, M., Gienapp, L., Euchner, F., Heilenk{\"o}tter, N., Weidmann, N.,
  Wachsmuth, H., Stein, B., Hagen, M.: {Argument Search: Assessing Argument
  Relevance}. In: 42nd International ACM Conference on Research and Development
  in Information Retrieval (SIGIR 2019). ACM (Jul 2019).
  \doi{10.1145/3331184.3331327},
  \url{http://doi.acm.org/10.1145/3331184.3331327}

\bibitem{rinott-etal-2015-show}
Rinott, R., Dankin, L., Alzate~Perez, C., Khapra, M.M., Aharoni, E., Slonim,
  N.: Show me your evidence - an automatic method for context dependent
  evidence detection. In: Proceedings of the 2015 Conference on Empirical
  Methods in Natural Language Processing. pp. 440--450. Association for
  Computational Linguistics, Lisbon, Portugal (Sep 2015).
  \doi{10.18653/v1/D15-1050}, \url{https://www.aclweb.org/anthology/D15-1050}

\bibitem{stab2018argumentext}
Stab, C., Daxenberger, J., Stahlhut, C., Miller, T., Schiller, B., Tauchmann,
  C., Eger, S., Gurevych, I.: Argumentext: Searching for arguments in
  heterogeneous sources. In: Proceedings of the 2018 conference of the North
  American chapter of the association for computational linguistics:
  demonstrations. pp. 21--25 (2018)

\bibitem{wachsmuth2017computational}
Wachsmuth, H., Naderi, N., Hou, Y., Bilu, Y., Prabhakaran, V., Thijm, T.A.,
  Hirst, G., Stein, B.: Computational argumentation quality assessment in
  natural language. In: Proceedings of the 15th Conference of the European
  Chapter of the Association for Computational Linguistics: Volume 1, Long
  Papers. pp. 176--187 (2017)

\bibitem{wachsmuth-etal-2017-building}
Wachsmuth, H., Potthast, M., Al-Khatib, K., Ajjour, Y., Puschmann, J., Qu, J.,
  Dorsch, J., Morari, V., Bevendorff, J., Stein, B.: Building an argument
  search engine for the web pp. 49--59 (Sep 2017). \doi{10.18653/v1/W17-5106},
  \url{https://www.aclweb.org/anthology/W17-5106}

\bibitem{warriner2013norms}
Warriner, A.B., Kuperman, V., Brysbaert, M.: Norms of valence, arousal, and
  dominance for 13,915 english lemmas. Behavior research methods
  \textbf{45}(4),  1191--1207 (2013)

\bibitem{wei2016post}
Wei, Z., Liu, Y., Li, Y.: Is this post persuasive? ranking argumentative
  comments in online forum. In: Proceedings of the 54th Annual Meeting of the
  Association for Computational Linguistics (Volume 2: Short Papers). pp.
  195--200 (2016)

\bibitem{zhu2007improving}
Zhu, X., Goldberg, A.B., Van~Gael, J., Andrzejewski, D.: Improving diversity in
  ranking using absorbing random walks. In: Human Language Technologies 2007:
  The Conference of the North American Chapter of the Association for
  Computational Linguistics; Proceedings of the Main Conference. pp. 97--104
  (2007)

\bibitem{zuccon2015integrating}
Zuccon, G., Koopman, B., Bruza, P., Azzopardi, L.: Integrating and evaluating
  neural word embeddings in information retrieval. In: Proceedings of the 20th
  Australasian document computing symposium. pp.~1--8 (2015)

\end{thebibliography}
