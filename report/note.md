# Report Structure

1. Abstract
2. Introduction
3. Related Work (Max/Matthias)
    - Grob Überblick über die Argument-Suchmaschinen-Architekturen
    - Args.me, ...
    - Pipelines anderen Suchmaschinen
    - Args.me Corpus beschreiben
4. Methods
   - unsere Architektur Überblick (Max)
   - Pipeline und Aufgaben der einzelnen Teile (Max)
  4.1. Preprocessing (Lukas ✅)
    Related Work
    Methods
    Results
  4.2. Retrieval Model (Matthias)
    Related Work !! DPH
    Methods
  4.3. Query Expansion (Max)
    Related Work
    Methods
  4.4. Clustering (Lukas)
    Related Work
    Methods
5. Evaluation & Results (Matthias)
   - TREC files of Tira.io / Kombination
   - Händisch Evalution
   - [Performance]…
6. Conclusion & Outlook
