% This is samplepaper.tex, a sample chapter demonstrating the
% LLNCS macro package for Springer Computer Science proceedings;
% Version 2.20 of 2017/10/04
%
\documentclass[runningheads]{llncs}
%
\usepackage{graphicx}
\usepackage[table]{xcolor}
% Used for displaying a sample figure. If possible, figure files should
% be included in EPS format.
%
% If you use the hyperref package, please uncomment the following line
% to display URLs in blue roman font according to Springer's eBook style:
% \renewcommand\UrlFont{\color{blue}\rmfamily}

\begin{document}
%


% 
\subsection{Preprocessing} 
The quality of the arguments contained in the corpus is heterogeneous. Some documents do not contain arguments at all. Therefore we aim to assign ratings to the documents indicating their argumentative quality. More formally, we create a mapping
\begin{equation}
q: D\rightarrow [0,1]
\end{equation}
where $D$ is the corpus as a set of documents. If $q(d_1)>q(d_2)$, the argumentative quality of $d_1$ is considered higher than that of $d_2$. \\ These ratings are then used in our retrieval model, as the user should only receive arguments with high quality. To compute these ratings, we employ a machine learning approach. 
\subsubsection{Related Work} 
$~$\\
Argument quality is a rather elusive concept that can not be quantified directly. Wachsmuth et al. \cite{wachsmuth2017computational} break it down into three aspects: 
\begin{itemize}
    \item \emph{Logical quality}: are the premises acceptable and do they really imply the conclusion?  
    \item \emph{Rhetorical quality}: is the argument formulated in a persuasive manner? 
    \item \emph{Dialectical quality}: does the argument contribute to resolving the issue?
\end{itemize} 
Wei et al. \cite{wei2016post} rank argumentative reddit posts in order to find the most persuasive ones. The authors use three types of features to predict an argument's persuasiveness: 
\begin{itemize}
    \item \emph{Surface Text Features}, among them average sentence length, number of contained URLs and number of punctuation marks 
    \item \emph{Social Interaction Features}, especially features of the post's "reply tree" 
    \item \emph{Argumentation Related Features}, including e.g. number of modal verbs and similarity with the original post 
\end{itemize} 
A combination of all feature types led to the best results, while the Surface Text Features alone performed worse than baseline methods. \\ 
Another approach to predict persuasiveness is proposed by Persing and Ng \cite{persing2017can}. They use 11 feature types, e.g. "subjectivity indicators", number of definite articles and number of citations.\\ 
Potthast et al. \cite{stein:2019j} provide a subset of the argsme corpus in which arguments were manually annotated with a rating for each of the three quality aspects defined by Wachsmuth et al. \cite{wachsmuth2017computational}. Furthermore, each argument was given an overall quality rating.
\subsubsection{Methods}
$~$\\
Capturing the logical dimension of argument quality with computational features is a hard task. Solving it is beyond the scope of this student project. The dialectical quality dimension is not available in our corpus either. Other than the corpus of reddit posts used by Wei et al. \cite{wei2016post}, the argsme corpus does not contain information about replies to a post or citations of a post. Thus, the only quality dimension (as defined by Wachsmuth et al. \cite{wachsmuth2017computational}) we aim to quantify is rhetorical quality.\\
To achieve this, we compute 22 features for each argument. Most of them can already be found in Wei et al. \cite{wei2016post} and Persing and Ng \cite{persing2017can}. In the following, the features are briefly described. \\
\textbf{Linguistic competence}: these features aim at quantifying the argument's author's linguistic skills: 
\begin{itemize}
    % 1
    \item \emph{avg. sentence length}: a more competent speaker will typically use more complex sentences
    % 2
    \item \emph{avg. word length}: a more competent speaker will typically use more complex/longer words
    % 4
    \item \emph{type/token ratio}: a more competent speaker has a more extensive vocabulary
    % 5
    \item \emph{\# punctuation marks per sentence}: a more competent speaker will more often use commas, hyphens and semicolons. We do not count exclamation marks and question marks, as their frequent usage may indicate high excitation and hence lack of objectivity 
    % 6 
    % TODO stimmt das überhaupt?
    \item \emph{number of different POS-Tags (whole text)}: a more competent speaker may use a wide range of different parts of speech  
    % 7
    \item \emph{\#conjunctives per sentence}: a more competent speaker typically uses conjunctives 
    % 8
    \item \emph{\#modal verbs per sentence}: a more competent speaker might use more modal verbs 
    % ?
    \item \emph{\# non-stopwords / \#tokens}: a high value for this feature may indicate a broader vocabulary 
    % 24
    \item \emph{\#emojis}: Use of emojis might coincide with rather colloquial language and lack of seriousness 
    
    
\end{itemize} 
\textbf{Sources and Examples}: 
claims are more persuasive when they are supported by examples and sources.
\begin{itemize}
    % 12
    \item \emph{\#references per sentence}: rule-based counting of references 
    % 16 
    \item \emph{\#examples per sentence}: rule-based counting of examples. An argument is more clear when supported with an illustrative example  
    % 3
    \item \emph{\#URLs per sentence}: URLs are often used to reference evidence for a claim. 
    % 23
    \item \emph{\#percentages per sentence}: Providing a concrete percentage may make a claim appear more credible 
    % 23
    \item \emph{\#year specifications per sentence}: Providing a concrete year specifications may make a claim appear more credible
\end{itemize} 

\textbf{Subjectivity, ad hominem and emotionality}: 
Arguments are more persuasive when they are presented in an objective manner, without anecdotal evidence or attacking the opponent personally. 
\begin{itemize} 
    % 11
    \item \emph{\#first person pl. pronouns per sentence} indicate subjectivity. We do not count first person singular words. Persing and Ng \cite{persing2017can} argue that objective arguments frequently start with phrases like "I think..." or "I believe...", too. 
    % 14
    \item \emph{\# second person pronouns per sentence} may indicate personal attacks 
    % 15
    \item \emph{Sentiment Analysis} is able to indicate high emotionality. We use VADER (Hutto et al. \cite{hutto2014vader}).  
    % 17
    \item \emph{\# hedge words/phrases per sentence}: these phrases may indicate a more polite, indirect and differentiated formulation. We use a list\footnote{https://github.com/words/hedges} to identify such phrases. 
    % 10
    \item \emph{\#definite articles / \#articles}: Persing and Ng \cite{persing2017can} argue that a lack of definite articles often means a lack of specifity and objectivity
    % 18 
    \item \emph{average concreteness}: Brysbaert et al. \cite{brysbaert2014concreteness} provide ratings for word concreteness obtained by crowd sourcing. This feature describes the average degree of abstractness in the argument. 
    \item \emph{components of emotions}: Words affect our emotions. According to Warriner et al. \cite{warriner2013norms}, there are three components of each emotion: 
    \begin{itemize}
        % 19
        \item valence, i.e. "pleasantness"(\cite{warriner2013norms}), ranges from "happy" to "unhappy"  
        % 20
        \item arousal is "the intensity of emotion provoked by a stimulus" (\cite{brysbaert2014concreteness})
        % 21
        \item dominance denotes "the degree of control exerted by a stimulus" (\cite{warriner2013norms})
    \end{itemize} For each of these emotion components, Warriner et al. provide word ratings. We build three features: average valence of words in the argument, average arousal and average dominance. 
    
\end{itemize}

Before min-max-normalizing all features we filter out odd documents according to table \ref{arg_rule_tab} and assign the rating 0.0 to them. Such documents are typically spam, argumentative "rap battles" or short meta-posts like e.g. "I accept", "Vote Pro" etc. The reason for the very high upper bound for sentence lengths (48.0) is that sometimes segmentation fails, mistaking a sequence of sentences for just one sentence.
\begin{table}
\caption{Only documents that fulfill these conditions are kept}\label{arg_rule_tab}
\begin{tabular}{|l|c|}
\hline
Feature &  allowed range\\
\hline
avg. sentence length &  $[3.0,48.0]$\\
avg. word length & $[2.0,16.0]$ \\ 
type-token-ratio & $[0.5,1.0]$ \\
\hline
\end{tabular}
\end{table} 

As training data we use the SIGIR-19-ArgSearch corpus by Potthast et al. \cite{stein:2019j}. It contains about 1600 arguments from the argsme corpus. Furthermore, it provides ratings for all three argument quality dimensions as well as for combined/overall argument quality. These continuous ratings range from -4.0 (not an argument) to 4.0.  

We train several several machine learning models on 75\% of the data: Linear Regression, Decision Tree Regression and SVR with several different kernels. For each type of model we train one instance on rhetorical quality (r) and another instance on combined quality (c). Both instances' parameters are optimized via grid search. 


\subsubsection{Results} 
$~$\\
The results are shown in table \ref{res_table}.
\begin{table}
\caption{Mean Squared Errors (MSE) of different models for rhetorical quality (r) and combined quality (c)}\label{res_table}
\begin{tabular}{|l|c|c|c|}
\hline
Model &  trained on & MSE (r) & MSE (c)\\
\hline 
\hline
Linear Regression & r & 1.655 & 1.524 \\
Linear Regression & c & 1.660 & 1.519 \\ 
\hline
Decision Tree Regression & r & 1.767 & 1.614 \\
Decision Tree Regression & c & 1.878 & 1.696 \\ 
\hline
SVR (poly) & r & 1.651 & 1.499 \\
SVR (poly) & c & 1.641 & 1.475 \\ 
\hline
SVR (rbf) & r & 1.737 & 1.578 \\
SVR (rbf) & c & 1.664 & 1.496 \\ 
\hline
SVR (sigmoid) & r & 1.719 & 1.590 \\
SVR (sigmoid) & c & 1.671 & 1.532 \\
\hline 
%Combination of all (equal weights) & r,c & 1.623 & 1.472\\ 
\textbf{Ensemble (LR)} & (predictions of models above) & \textbf{1.468} & \textbf{1.322} \\
\hline
\end{tabular}
\end{table} 
All models perform rather poorly, indicating that predicting argument quality is a difficult problem. SVR with a polynomial kernel (degree 2) achieves the best results. Moreover, we train an ensemble model using the predictions of all models as features (last row in table \ref{res_table}). Linear Regression proved to yield the best results here. As expected, the ensemble method outperforms all single models. 

An interesting detail is that all models, even those trained on rhetorical quality, perform better in predicting combined argument quality (MSE(c) $<$ MSE(r)). In other words, overall quality seems to be easier to predict than rhetorical quality, at least with our approach. This hypothesis is statistically significant for $p<0.01$. One explanation for this observation may be that some of our features also capture aspects of dialectical and logical quality: For example, providing sources to support a claim could indicate logical correctness. Features related to subjectivity and emotionality might at least be able to suggest low dialectical quality, as a very emotional and/or subjective post is often unlikely to contribute to resolving an issue. 

Analysing the coefficients of the simple linear regression models (rows 1 and 2 of table \ref{res_table}) partly supports this reasoning and provides further insights. Linear regression assigns a coefficient $c_f$ to every feature $f$ from the feature set $F$. Table \ref{coef_table} shows for both linear regression models (i.e. rhetorical and combined quality) the impact of each of our 22 features. Hereby, "impact" of a feature $f$ is defined as $sgn(c_f)*\frac{|c_f|}{\Sigma_{g\in F}|c_{g}|}$.  A feature $f$ with $c_f<0$ has negative impact on the respective quality prediction, the opposite holds when $c_f>0$. All feature values are min-max-normalized.

\begin{table}
\caption{Mean Squared Errors (MSE) of different models for rhetorical quality (r) and combined quality (c)}\label{coef_table} 
\begin{tabular}[h]{|l|l|l|}
\hline
Feature $f$ &  $impact(f)$ (r) & $impact(f)$ (c)\\
\hline 
% coloring 
% r+ : * 4 
%r-: *7 
% c+: *5 
% c-: *7.5
avg. sent. length & \cellcolor{green!4}+.010 & +.009 \cellcolor{green!5} \\
avg. word length & \cellcolor{green!16}+.041 & +.039 \cellcolor{green!20} \\
type/token ratio & \cellcolor{red!4}-.006 & -.005 \cellcolor{red!4} \\
punct. marks / sent. & \cellcolor{red!70} \textcolor{white}{-.101} & \textcolor{white}{-.078}  \cellcolor{red!59} \\
\#different POS-Tags & \cellcolor{green!18}+.045 & +.039 \cellcolor{green!20} \\
\#conjunctives / sent. & \cellcolor{green!10}+.025 & +.021 \cellcolor{green!10} \\
\#modal verbs / sent. &\cellcolor{red!4}-.006 & -.005 \cellcolor{red!4} \\
\#non-stopwords / \#tokens & \cellcolor{red!1}-.001 & -.003  \cellcolor{red!2} \\
\#emojis & \cellcolor{red!14}-.021 & \textcolor{white}{-.112}  \cellcolor{red!84} \\
\#references / sent. & \cellcolor{red}\textcolor{white}{-.142} & \textcolor{white}{-.114}  \cellcolor{red!86} \\
\#examples / sent. & \cellcolor{red!2}-.003 & -.003  \cellcolor{red!2}\\
\#URLs / sent. & \cellcolor{green}+.261 & +.205 \cellcolor{green}\\
\#percentages / sent. & -.000 & -.000 \\
\#year spec. / sentence & \cellcolor{green!84}+.210 & +.198 \cellcolor{green!98}\\
\#$1^{st}$ p. pl. pronouns / sent. & \cellcolor{red!54}\textcolor{white}{-.078} & \textcolor{white}{-.134}  \cellcolor{red} \\
\# $2^{nd}$ p. pronouns / sent. & \cellcolor{red!21}-.030 & -.026  \cellcolor{red!20} \\
\#hedge words/phrases / sent. &-.000 & -.002  \cellcolor{red!2} \\
\#def. articles / \#articles & \cellcolor{green!1}+.002 & +.002 \cellcolor{green!1}\\
avg. concreteness & \cellcolor{red!8}-.011 & -.004  \cellcolor{red!3} \\ 
avg. valence & \cellcolor{green!12} +.028 & +.022 \cellcolor{green!11} \\
avg. arousal & \cellcolor{green!1}+.001 & -.000 \\
avg. dominance & \cellcolor{green!3}+.004 & +.002 \cellcolor{green!1}\\

sentiment & \cellcolor{red!1}-.001 & -.001  \cellcolor{red!1} \\
\hline
\end{tabular}
\end{table} 
We briefly note some interesting aspects: 
\begin{itemize}
    \item URLs and year specifications both have heavy positive influence on the scores for rhetorical as well as combined quality. As argued above, they might make an argument more credible. 
    \item Surprisingly, the \#references/sent. feature, belonging to the same feature group as URLs and year specifications, has considerably negative influence on both scores.  
    \item As expected, many of the features intended to describe linguistic competence have positive impact on both rhetorical and overall quality, although many of them are not very important. A remarkable exception from this is the number of punctuation marks (commas, hyphens, semicolons) per sentence. Maybe too complex sentences, containing many punctuation marks, are judged as rhetorically bad and thus also of lower overall quality. 
    \item Emojis have negative impact on both quality dimensions. The effect is way stronger for combined/overall quality. This might indicate that emojis often occur in non-arguments. 
    \item The features capturing subjectivity (first and second person pronouns) worsen argument quality, which comes as no surprise. This effect is especially notable for first person plural pronouns in combined/overall quality. A possible explanation may be that subjectivity might not necessarily impair rhetorical quality, while the overall quality of an argument strongly depends on objectivity. 
    \item Sentiment Analysis and the psycholinguistic features arousal, dominance and concreteness are irrelevant. The only exception is valence, i.e. pleasantness of a word/text. An argument might be considered slightly better (rhetorically as well as overall) by annotators if it contains many words that subconsciously evoke positive feelings.
\end{itemize} 

Finally, to obtain the desired quality function $q:D\rightarrow [0,1]$, we let the trained models predict the combined quality of every argument in the argsme corpus. We compute the predictions of the best single model (quadratic SVR trained on combined quality) and the ensemble model, leading to two candidates $q_{svr},q_{ens}$ for $q$. Figure \ref{dist} shows the distributions of the ratings generated by both models. 

\begin{figure}
\centering
\caption{Distributions of ratings produced by ensemble classifier ($q_{ens}$) and SVR classifier ($q_{svr}$)}\label{dist} 
\includegraphics[width=25em]{comparison.png}

\end{figure}

We choose $q_{svr}$ as quality function $q$, even though the MSE of the SVR model is higher than that of the ensemble method. The main reason for this decision is that there are almost no "bad" arguments according to $q_{ens}$, which is certainly inaccurate.


%
% ---- Bibliography ----
%
% BibTeX users should specify bibliography style 'splncs04'.
% References will then be sorted and formatted in the correct style.
%
\bibliographystyle{splncs04}
\bibliography{bib}

\end{document}
